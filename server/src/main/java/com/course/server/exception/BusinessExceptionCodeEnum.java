package com.course.server.exception;

/**
 * 自定义业务异常信息枚举
 * @author cl
 * @date 2020/9/28 12:52
 */
public enum BusinessExceptionCodeEnum {
    USER_LOGIN_NAME_EXIST("登录名已存在"),
    USER_LOGIN_ERROR("用户名或密码不存在"),
    ;

    private String desc;

    BusinessExceptionCodeEnum(String desc) {
        this.desc = desc;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }
}
