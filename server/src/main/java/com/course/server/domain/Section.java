package com.course.server.domain;

import java.util.Date;

public class Section {
    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column section.id
     *
     * @mbg.generated
     */
    private String id;

    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column section.title
     *
     * @mbg.generated
     */
    private String title;

    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column section.course_id
     *
     * @mbg.generated
     */
    private String courseId;

    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column section.chapter_id
     *
     * @mbg.generated
     */
    private String chapterId;

    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column section.video
     *
     * @mbg.generated
     */
    private String video;

    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column section.time
     *
     * @mbg.generated
     */
    private Integer time;

    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column section.charge
     *
     * @mbg.generated
     */
    private String charge;

    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column section.sort
     *
     * @mbg.generated
     */
    private Integer sort;

    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column section.create_user
     *
     * @mbg.generated
     */
    private String createUser;

    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column section.create_time
     *
     * @mbg.generated
     */
    private Date createTime;

    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column section.update_user
     *
     * @mbg.generated
     */
    private String updateUser;

    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column section.update_time
     *
     * @mbg.generated
     */
    private Date updateTime;

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column section.id
     *
     * @return the value of section.id
     *
     * @mbg.generated
     */
    public String getId() {
        return id;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column section.id
     *
     * @param id the value for section.id
     *
     * @mbg.generated
     */
    public void setId(String id) {
        this.id = id == null ? null : id.trim();
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column section.title
     *
     * @return the value of section.title
     *
     * @mbg.generated
     */
    public String getTitle() {
        return title;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column section.title
     *
     * @param title the value for section.title
     *
     * @mbg.generated
     */
    public void setTitle(String title) {
        this.title = title == null ? null : title.trim();
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column section.course_id
     *
     * @return the value of section.course_id
     *
     * @mbg.generated
     */
    public String getCourseId() {
        return courseId;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column section.course_id
     *
     * @param courseId the value for section.course_id
     *
     * @mbg.generated
     */
    public void setCourseId(String courseId) {
        this.courseId = courseId == null ? null : courseId.trim();
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column section.chapter_id
     *
     * @return the value of section.chapter_id
     *
     * @mbg.generated
     */
    public String getChapterId() {
        return chapterId;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column section.chapter_id
     *
     * @param chapterId the value for section.chapter_id
     *
     * @mbg.generated
     */
    public void setChapterId(String chapterId) {
        this.chapterId = chapterId == null ? null : chapterId.trim();
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column section.video
     *
     * @return the value of section.video
     *
     * @mbg.generated
     */
    public String getVideo() {
        return video;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column section.video
     *
     * @param video the value for section.video
     *
     * @mbg.generated
     */
    public void setVideo(String video) {
        this.video = video == null ? null : video.trim();
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column section.time
     *
     * @return the value of section.time
     *
     * @mbg.generated
     */
    public Integer getTime() {
        return time;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column section.time
     *
     * @param time the value for section.time
     *
     * @mbg.generated
     */
    public void setTime(Integer time) {
        this.time = time;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column section.charge
     *
     * @return the value of section.charge
     *
     * @mbg.generated
     */
    public String getCharge() {
        return charge;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column section.charge
     *
     * @param charge the value for section.charge
     *
     * @mbg.generated
     */
    public void setCharge(String charge) {
        this.charge = charge == null ? null : charge.trim();
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column section.sort
     *
     * @return the value of section.sort
     *
     * @mbg.generated
     */
    public Integer getSort() {
        return sort;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column section.sort
     *
     * @param sort the value for section.sort
     *
     * @mbg.generated
     */
    public void setSort(Integer sort) {
        this.sort = sort;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column section.create_user
     *
     * @return the value of section.create_user
     *
     * @mbg.generated
     */
    public String getCreateUser() {
        return createUser;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column section.create_user
     *
     * @param createUser the value for section.create_user
     *
     * @mbg.generated
     */
    public void setCreateUser(String createUser) {
        this.createUser = createUser == null ? null : createUser.trim();
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column section.create_time
     *
     * @return the value of section.create_time
     *
     * @mbg.generated
     */
    public Date getCreateTime() {
        return createTime;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column section.create_time
     *
     * @param createTime the value for section.create_time
     *
     * @mbg.generated
     */
    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column section.update_user
     *
     * @return the value of section.update_user
     *
     * @mbg.generated
     */
    public String getUpdateUser() {
        return updateUser;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column section.update_user
     *
     * @param updateUser the value for section.update_user
     *
     * @mbg.generated
     */
    public void setUpdateUser(String updateUser) {
        this.updateUser = updateUser == null ? null : updateUser.trim();
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column section.update_time
     *
     * @return the value of section.update_time
     *
     * @mbg.generated
     */
    public Date getUpdateTime() {
        return updateTime;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column section.update_time
     *
     * @param updateTime the value for section.update_time
     *
     * @mbg.generated
     */
    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table section
     *
     * @mbg.generated
     */
    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", id=").append(id);
        sb.append(", title=").append(title);
        sb.append(", courseId=").append(courseId);
        sb.append(", chapterId=").append(chapterId);
        sb.append(", video=").append(video);
        sb.append(", time=").append(time);
        sb.append(", charge=").append(charge);
        sb.append(", sort=").append(sort);
        sb.append(", createUser=").append(createUser);
        sb.append(", createTime=").append(createTime);
        sb.append(", updateUser=").append(updateUser);
        sb.append(", updateTime=").append(updateTime);
        sb.append("]");
        return sb.toString();
    }
}