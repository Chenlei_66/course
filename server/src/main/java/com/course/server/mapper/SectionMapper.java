package com.course.server.mapper;

import com.course.server.domain.Section;
import com.course.server.domain.SectionExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface SectionMapper {
    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table section
     *
     * @mbg.generated
     */
    long countByExample(SectionExample example);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table section
     *
     * @mbg.generated
     */
    int deleteByExample(SectionExample example);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table section
     *
     * @mbg.generated
     */
    int deleteByPrimaryKey(String id);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table section
     *
     * @mbg.generated
     */
    int insert(Section record);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table section
     *
     * @mbg.generated
     */
    int insertSelective(Section record);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table section
     *
     * @mbg.generated
     */
    List<Section> selectByExample(SectionExample example);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table section
     *
     * @mbg.generated
     */
    Section selectByPrimaryKey(String id);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table section
     *
     * @mbg.generated
     */
    int updateByExampleSelective(@Param("record") Section record, @Param("example") SectionExample example);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table section
     *
     * @mbg.generated
     */
    int updateByExample(@Param("record") Section record, @Param("example") SectionExample example);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table section
     *
     * @mbg.generated
     */
    int updateByPrimaryKeySelective(Section record);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table section
     *
     * @mbg.generated
     */
    int updateByPrimaryKey(Section record);
}