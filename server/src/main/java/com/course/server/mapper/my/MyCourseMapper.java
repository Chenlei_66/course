package com.course.server.mapper.my;

import com.course.server.dto.SortDTO;

/**
 * 自定义CourseMapper
 */
public interface MyCourseMapper {

    /**
     * 更新课程时长
     * @param courseId
     * @return
     */
    int updateTime(String courseId);

    /**
     * 更新课程排序
     * @param sortDTO
     * @return
     */
    int updateSort(SortDTO sortDTO);

    /**
     * 向后移动课程排序
     * @param sortDTO
     * @return
     */
    int moveSortsBackward(SortDTO sortDTO);

    /**
     * 向前移动课程排序
     * @param sortDTO
     * @return
     */
    int moveSortsForward(SortDTO sortDTO);
}