package com.course.server.mapper.my;

import com.course.server.dto.ResourceDTO;

import java.util.List;

/**
 * 自定义CourseMapper
 */
public interface MyUserMapper {

    /**
     * 查询用户所有资源信息
     * @param userId
     * @return
     */
    List<ResourceDTO> findResources(String userId);
}