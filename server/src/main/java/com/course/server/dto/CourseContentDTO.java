package com.course.server.dto;

import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;

public class CourseContentDTO {

    /**
     * 课程id
     */
     private String id;

    /**
     * 课程内容
     */
     private String content;

    /**
     * 创建人
     */
     private String createUser;

    /**
     * 创建时间
     */
     @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
     private Date createTime;

    /**
     * 修改人
     */
     private String updateUser;

    /**
     * 修改时间
     */
     @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
     private Date updateTime;

    public String getId(){
        return id;
    }

    public void setId(String id){
        this.id = id;
    }
    public String getContent(){
        return content;
    }

    public void setContent(String content){
        this.content = content;
    }
    public String getCreateUser(){
        return createUser;
    }

    public void setCreateUser(String createUser){
        this.createUser = createUser;
    }
    public Date getCreateTime(){
        return createTime;
    }

    public void setCreateTime(Date createTime){
        this.createTime = createTime;
    }
    public String getUpdateUser(){
        return updateUser;
    }

    public void setUpdateUser(String updateUser){
        this.updateUser = updateUser;
    }
    public Date getUpdateTime(){
        return updateTime;
    }

    public void setUpdateTime(Date updateTime){
        this.updateTime = updateTime;
    }


    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(",id=").append(id);
        sb.append(",content=").append(content);
        sb.append(",createUser=").append(createUser);
        sb.append(",createTime=").append(createTime);
        sb.append(",updateUser=").append(updateUser);
        sb.append(",updateTime=").append(updateTime);
        sb.append("]");
        return sb.toString();
    }
}