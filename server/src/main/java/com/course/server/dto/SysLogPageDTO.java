package com.course.server.dto;

/**
 * @author cl
 * @description
 * @date 2020/8/5 13:00
 */
public class SysLogPageDTO extends PageDTO<SysLogDTO>{
    private String businessId;

    private String result;

    public String getBusinessId() {
        return businessId;
    }

    public void setBusinessId(String businessId) {
        this.businessId = businessId;
    }

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }

    @Override
    public String toString() {
        final StringBuffer sb = new StringBuffer("SysLogPageDTO{");
        sb.append("businessId='").append(businessId).append('\'');
        sb.append(", result='").append(result).append('\'');
        sb.append(", page=").append(page);
        sb.append(", size=").append(size);
        sb.append(", total=").append(total);
        sb.append(", list=").append(list);
        sb.append('}');
        return sb.toString();
    }
}
