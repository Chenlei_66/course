package com.course.server.dto;

import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;

public class TeacherDTO {

    /**
     * id
     */
     private String id;

    /**
     * 姓名
     */
     private String name;

    /**
     * 昵称
     */
     private String nickName;

    /**
     * 头像
     */
     private String image;

    /**
     * 职位
     */
     private String position;

    /**
     * 座右铭
     */
     private String motto;

    /**
     * 简介
     */
     private String intro;

    /**
     * 创建人
     */
     private String createUser;

    /**
     * 创建时间
     */
     @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
     private Date createTime;

    /**
     * 修改人
     */
     private String updateUser;

    /**
     * 修改时间
     */
     @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
     private Date updateTime;

    public String getId(){
        return id;
    }

    public void setId(String id){
        this.id = id;
    }
    public String getName(){
        return name;
    }

    public void setName(String name){
        this.name = name;
    }
    public String getNickName(){
        return nickName;
    }

    public void setNickName(String nickName){
        this.nickName = nickName;
    }
    public String getImage(){
        return image;
    }

    public void setImage(String image){
        this.image = image;
    }
    public String getPosition(){
        return position;
    }

    public void setPosition(String position){
        this.position = position;
    }
    public String getMotto(){
        return motto;
    }

    public void setMotto(String motto){
        this.motto = motto;
    }
    public String getIntro(){
        return intro;
    }

    public void setIntro(String intro){
        this.intro = intro;
    }
    public String getCreateUser(){
        return createUser;
    }

    public void setCreateUser(String createUser){
        this.createUser = createUser;
    }
    public Date getCreateTime(){
        return createTime;
    }

    public void setCreateTime(Date createTime){
        this.createTime = createTime;
    }
    public String getUpdateUser(){
        return updateUser;
    }

    public void setUpdateUser(String updateUser){
        this.updateUser = updateUser;
    }
    public Date getUpdateTime(){
        return updateTime;
    }

    public void setUpdateTime(Date updateTime){
        this.updateTime = updateTime;
    }


    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(",id=").append(id);
        sb.append(",name=").append(name);
        sb.append(",nickName=").append(nickName);
        sb.append(",image=").append(image);
        sb.append(",position=").append(position);
        sb.append(",motto=").append(motto);
        sb.append(",intro=").append(intro);
        sb.append(",createUser=").append(createUser);
        sb.append(",createTime=").append(createTime);
        sb.append(",updateUser=").append(updateUser);
        sb.append(",updateTime=").append(updateTime);
        sb.append("]");
        return sb.toString();
    }
}