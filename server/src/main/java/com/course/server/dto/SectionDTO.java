package com.course.server.dto;

import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;

public class SectionDTO {

    /**
     * ID
     */
     private String id;

    /**
     * 标题
     */
     private String title;

    /**
     * 课程|course.id
     */
     private String courseId;

    /**
     * 大章|chapter.id
     */
     private String chapterId;

    /**
     * 视频
     */
     private String video;

    /**
     * 时长|单位秒
     */
     private Integer time;

    /**
     * 收费|C 收费；F 免费
     */
     private String charge;

    /**
     * 顺序
     */
     private Integer sort;

    /**
     * 创建人
     */
     private String createUser;

    /**
     * 创建时间
     */
     @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
     private Date createTime;

    /**
     * 修改人
     */
     private String updateUser;

    /**
     * 修改时间
     */
     @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
     private Date updateTime;

    public String getId(){
        return id;
    }

    public void setId(String id){
        this.id = id;
    }
    public String getTitle(){
        return title;
    }

    public void setTitle(String title){
        this.title = title;
    }
    public String getCourseId(){
        return courseId;
    }

    public void setCourseId(String courseId){
        this.courseId = courseId;
    }
    public String getChapterId(){
        return chapterId;
    }

    public void setChapterId(String chapterId){
        this.chapterId = chapterId;
    }
    public String getVideo(){
        return video;
    }

    public void setVideo(String video){
        this.video = video;
    }
    public Integer getTime(){
        return time;
    }

    public void setTime(Integer time){
        this.time = time;
    }
    public String getCharge(){
        return charge;
    }

    public void setCharge(String charge){
        this.charge = charge;
    }
    public Integer getSort(){
        return sort;
    }

    public void setSort(Integer sort){
        this.sort = sort;
    }
    public String getCreateUser(){
        return createUser;
    }

    public void setCreateUser(String createUser){
        this.createUser = createUser;
    }
    public Date getCreateTime(){
        return createTime;
    }

    public void setCreateTime(Date createTime){
        this.createTime = createTime;
    }
    public String getUpdateUser(){
        return updateUser;
    }

    public void setUpdateUser(String updateUser){
        this.updateUser = updateUser;
    }
    public Date getUpdateTime(){
        return updateTime;
    }

    public void setUpdateTime(Date updateTime){
        this.updateTime = updateTime;
    }


    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(",id=").append(id);
        sb.append(",title=").append(title);
        sb.append(",courseId=").append(courseId);
        sb.append(",chapterId=").append(chapterId);
        sb.append(",video=").append(video);
        sb.append(",time=").append(time);
        sb.append(",charge=").append(charge);
        sb.append(",sort=").append(sort);
        sb.append(",createUser=").append(createUser);
        sb.append(",createTime=").append(createTime);
        sb.append(",updateUser=").append(updateUser);
        sb.append(",updateTime=").append(updateTime);
        sb.append("]");
        return sb.toString();
    }
}