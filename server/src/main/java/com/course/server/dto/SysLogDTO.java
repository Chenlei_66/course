package com.course.server.dto;

import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;

public class SysLogDTO {

   /**
    * id
    */
    private String id;

   /**
    * 业务流水号
    */
    private String businessId;

   /**
    * 操作描述
    */
    private String description;

   /**
    * 请求ip
    */
    private String ip;

   /**
    * 服务名
    */
    private String service;

   /**
    * 方法名
    */
    private String method;

   /**
    * 处理结果|枚举[OptionResultEnum]：YES("1", "成功"),NO("2", "失败");
    */
    private String result;

   /**
    * 操作人
    */
    private String user;

   /**
    * 消耗时间
    */
    private Integer consumeTime;

   /**
    * 创建人
    */
    private String createUser;

   /**
    * 创建时间
    */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
    private Date createTime;

   /**
    * 修改人
    */
    private String updateUser;

   /**
    * 修改时间
    */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
    private Date updateTime;

    public String getId(){
        return id;
    }

    public void setId(String id){
        this.id = id;
    }
    public String getBusinessId(){
        return businessId;
    }

    public void setBusinessId(String businessId){
        this.businessId = businessId;
    }
    public String getDescription(){
        return description;
    }

    public void setDescription(String description){
        this.description = description;
    }
    public String getIp(){
        return ip;
    }

    public void setIp(String ip){
        this.ip = ip;
    }
    public String getService(){
        return service;
    }

    public void setService(String service){
        this.service = service;
    }
    public String getMethod(){
        return method;
    }

    public void setMethod(String method){
        this.method = method;
    }
    public String getResult(){
        return result;
    }

    public void setResult(String result){
        this.result = result;
    }
    public String getUser(){
        return user;
    }

    public void setUser(String user){
        this.user = user;
    }
    public Integer getConsumeTime(){
        return consumeTime;
    }

    public void setConsumeTime(Integer consumeTime){
        this.consumeTime = consumeTime;
    }
    public String getCreateUser(){
        return createUser;
    }

    public void setCreateUser(String createUser){
        this.createUser = createUser;
    }
    public Date getCreateTime(){
        return createTime;
    }

    public void setCreateTime(Date createTime){
        this.createTime = createTime;
    }
    public String getUpdateUser(){
        return updateUser;
    }

    public void setUpdateUser(String updateUser){
        this.updateUser = updateUser;
    }
    public Date getUpdateTime(){
        return updateTime;
    }

    public void setUpdateTime(Date updateTime){
        this.updateTime = updateTime;
    }


    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(",id=").append(id);
        sb.append(",businessId=").append(businessId);
        sb.append(",description=").append(description);
        sb.append(",ip=").append(ip);
        sb.append(",service=").append(service);
        sb.append(",method=").append(method);
        sb.append(",result=").append(result);
        sb.append(",user=").append(user);
        sb.append(",consumeTime=").append(consumeTime);
        sb.append(",createUser=").append(createUser);
        sb.append(",createTime=").append(createTime);
        sb.append(",updateUser=").append(updateUser);
        sb.append(",updateTime=").append(updateTime);
        sb.append("]");
        return sb.toString();
    }
}