package com.course.server.dto;

import java.util.HashSet;
import java.util.List;

public class LoginUserDTO {

   /**
    * id
    */
    private String id;

   /**
    * 登录名
    */
    private String loginName;

   /**
    * 昵称
    */
    private String name;

    /**
     * 登录凭证
     */
    private String token;

    /**
     * 所有资源，用于前端页面控制
     */
    private List<ResourceDTO> resources;
    /**
     * 所有资源的请求，用户后端接口拦截
     */
    private HashSet<String> requests;


    public String getId(){
        return id;
    }

    public void setId(String id){
        this.id = id;
    }
    public String getLoginName(){
        return loginName;
    }

    public void setLoginName(String loginName){
        this.loginName = loginName;
    }
    public String getName(){
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public List<ResourceDTO> getResources() {
        return resources;
    }

    public void setResources(List<ResourceDTO> resources) {
        this.resources = resources;
    }

    public HashSet<String> getRequests() {
        return requests;
    }

    public void setRequests(HashSet<String> requests) {
        this.requests = requests;
    }

    @Override
    public String toString() {
        final StringBuffer sb = new StringBuffer("LoginUserDTO{");
        sb.append("id='").append(id).append('\'');
        sb.append(", loginName='").append(loginName).append('\'');
        sb.append(", name='").append(name).append('\'');
        sb.append(", token='").append(token).append('\'');
        sb.append(", resources=").append(resources);
        sb.append(", requests=").append(requests);
        sb.append('}');
        return sb.toString();
    }
}