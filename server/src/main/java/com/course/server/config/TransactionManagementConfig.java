package com.course.server.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.transaction.annotation.EnableTransactionManagement;

/**
 * @author cl
 * @description
 * @date 2020/8/5 23:20
 */
@EnableTransactionManagement
@Configuration
public class TransactionManagementConfig {

}
