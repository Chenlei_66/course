package com.course.server.aop;

import com.alibaba.fastjson.JSONObject;
import com.course.server.annotation.LogToDb;
import com.course.server.enums.OperationResultEnum;
import com.course.server.service.SysLogService;
import com.course.server.util.ExceptionUtil;
import com.course.server.util.IpUtil;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.aspectj.lang.reflect.MethodSignature;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

/**
 * 记录日志到数据库
 *
 * @author cl
 * @description
 * @date 2020/9/18 15:43
 */
@Aspect
@Component
public class LogToDbAspect {

    /**
     * 获取服务名称
     */
    @Value("${spring.application.name}")
    private String serverName;

    /**
     * 与当前线程绑定避免多线程问题
     */
    ThreadLocal<Long> currentTime = new ThreadLocal<>();

    @Resource
    private SysLogService sysLogService;

    /**
     * 将@LogToDb注解设为切点
     */
    @Pointcut("@annotation(com.course.server.annotation.LogToDb)")
    public void logPointCut() {
    }


    /**
     * 环绕增强保存成功的操作日志信息
     *
     * @param point 切入点
     */
    @Around("logPointCut()")
    public Object around(ProceedingJoinPoint point) throws Throwable {
        long startTime = System.currentTimeMillis();
        currentTime.set(System.currentTimeMillis());
        HttpServletRequest request = ((ServletRequestAttributes) Objects.requireNonNull(RequestContextHolder
                .getRequestAttributes())).getRequest();
        Map<String, Object> requestMap = convertRequestMap(point);
        String ip = IpUtil.getLocalIp();
        String userNo = getUserNo(request);

        Object response;
        try {
            response = point.proceed();
            long consumeTime = System.currentTimeMillis() - startTime;
            sysLogService.saveLogs(ip, serverName, userNo, consumeTime,
                    response.toString(), OperationResultEnum.YES.getCode(), requestMap);
            return response;
        } catch (Exception e) {
            long consumeTime = System.currentTimeMillis() - startTime;
            sysLogService.saveLogs(ip, serverName, userNo, consumeTime,
                    ExceptionUtil.getStackTrace(e), OperationResultEnum.NO.getCode(), requestMap);
            throw e;
        }

    }

    /**
     * 返回map类型请求信息
     *
     * @param point 切入点
     */
    private Map<String, Object> convertRequestMap(JoinPoint point) {
        Map<String, Object> map = new HashMap<>();
        // 从切面织入点处通过反射机制获取织入点处的方法
        MethodSignature signature = (MethodSignature) point.getSignature();
        // 获取切入点所在的方法
        Method method = signature.getMethod();
        // 获取方法上的注解
        LogToDb logToDb = method.getAnnotation(LogToDb.class);
        if (logToDb != null) {
            // 从注解中获取参数
            map.put("logRecordDesc", logToDb.value().getDesc());
        }
        // 获取方法名(类名.方法名)
        String className = point.getTarget().getClass().getName();
        String methodName = method.getName();
        methodName = className + "." + methodName;
        map.put("methodName", methodName);

        // 获取请求参数并遍历
        Object[] args = point.getArgs();
        Object[] arguments = new Object[args.length];
        for (int i = 0; i < args.length; i++) {
            if (args[i] instanceof ServletRequest
                    || args[i] instanceof ServletResponse
                    || args[i] instanceof MultipartFile) {
                continue;
            }
            arguments[i] = args[i];
        }
        String requestStr = JSONObject.toJSONString(arguments);
        map.put("request", requestStr);

        return map;
    }

    /**
     * 获取UserNo
     *
     * @param request httpServletRequest
     */
    private String getUserNo(HttpServletRequest request) {
        return "system";
    }
}
