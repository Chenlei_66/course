package com.course.server.util;

import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;

/**
 * @author cl
 * @date 2020/9/24 15:23
 */
public class ExceptionUtil {

    /**
     * 获取异常完整堆栈信息
     *
     * @param t 异常
     * @return 异常的堆栈信息
     */
    public static String getStackTrace(Throwable t) {
        try (StringWriter sw = new StringWriter();
             PrintWriter pw = new PrintWriter(sw, true)) {
            t.printStackTrace(pw);
            return sw.getBuffer().toString();
        } catch (IOException e) {
            return String.valueOf(t);
        }
    }
}
