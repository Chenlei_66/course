package com.course.server.util;

import com.course.server.exception.ValidatorException;
import org.springframework.util.StringUtils;

public class ValidatorUtil {

    /**
     * 空校验
     * @param str 需校验字符串
     * @param fieldName 字符名称
     * @throws ValidatorException 自定义校验异常
     */
    public static void require(String str,String fieldName) {
        if(StringUtils.isEmpty(str)){
            throw new ValidatorException(fieldName + "不能为空");
        }
    }

    /**
     * 长度校验
     * @param str 需校验字符串
     * @param fieldName 字符名称
     * @param min 最小长度
     * @param max 最大长度
     * @throws ValidatorException 自定义校验异常
     */
    public static void length(String str,String fieldName,int min,int max) {
        if(StringUtils.isEmpty(str)){
            return;
        }
        int length = 0;
        if(!StringUtils.isEmpty(str)){
            length = str.length();
        }
        if(length < min || length > max){
            throw new ValidatorException(fieldName + "长度" + min + "~" + max + "位");
        }
    }
}
