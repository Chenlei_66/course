package com.course.server.util;

import org.springframework.util.CollectionUtils;

import java.net.*;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;

/**
 * ip地址工具类
 * @author cl
 * @date 2020/9/25 14:33
 */
public class IpUtil {

    /**
     * 获取本地ip地址
     */
    public static String getHostAddress() {
        String hostAddress = "0.0.0.0";
        try {
            InetAddress address = InetAddress.getLocalHost();
            hostAddress = address.getHostAddress();

        } catch (UnknownHostException e) {
            return hostAddress;
        }
        return hostAddress;
    }

    /**
     * 获取服务器IP地址
     * @return
     */
    public static String  getServerIp(){
        String SERVER_IP = "0.0.0.0";
        try {
            Enumeration<NetworkInterface> netInterfaces = NetworkInterface.getNetworkInterfaces();
            InetAddress ip;
            while (netInterfaces.hasMoreElements()) {
                NetworkInterface ni = netInterfaces.nextElement();
                ip = ni.getInetAddresses().nextElement();
                SERVER_IP = ip.getHostAddress();
                if (!ip.isSiteLocalAddress() && !ip.isLoopbackAddress()
                        && !ip.getHostAddress().contains(":")) {
                    SERVER_IP = ip.getHostAddress();
                    break;
                }
            }
        } catch (SocketException e) {
            return SERVER_IP;
        }

        return SERVER_IP;
    }

    /**
     * 获取本机IP地址
     *
     * @return
     */
    public static String getLocalIp() {
        String ip = "";
        List<String> ipList = getLocalIPv4List();
        String ipPrefix = "10";
        if (!CollectionUtils.isEmpty(ipList)) {
            for (String ipv4 : ipList) {
                if (ipv4.startsWith(ipPrefix)) {
                    ip = ipv4;
                    break;
                }
            }
        }

        return ip;
    }

    /**
     * 获取IPv4地址列表
     *
     * @return
     */
    public static List<String> getLocalIPv4List() {
        List<String> ipList = new ArrayList<String>();
        try {
            Enumeration<NetworkInterface> networkInterfaces = NetworkInterface
                    .getNetworkInterfaces();
            NetworkInterface networkInterface;
            Enumeration<InetAddress> inetAddresses;
            InetAddress inetAddress;
            String ip;
            while (networkInterfaces.hasMoreElements()) {
                networkInterface = networkInterfaces.nextElement();
                inetAddresses = networkInterface.getInetAddresses();
                while (inetAddresses.hasMoreElements()) {
                    inetAddress = inetAddresses.nextElement();
                    if (inetAddress != null
                            && inetAddress instanceof Inet4Address) { // IPV4
                        ip = inetAddress.getHostAddress();
                        ipList.add(ip);
                    }
                }
            }
        } catch (SocketException e) {
            e.printStackTrace();
        }
        return ipList;
    }

}
