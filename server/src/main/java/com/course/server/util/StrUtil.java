package com.course.server.util;

/**
 * 字符串工具类
 * @author cl
 * @date 2020/9/25 8:55
 */
public class StrUtil {

    /**
     * 从json字符串中截取指定name的value
     * @param jsonStr
     * @param name
     * @return
     */
    public static String subStrFromJsonStr(String jsonStr,String name){
        if (jsonStr == null || !jsonStr.contains(name)){
            return null;
        }
        String temp = jsonStr.substring(jsonStr.indexOf(name) + name.length() + 3);
        return temp.substring(0,temp.indexOf("\""));
    }
}
