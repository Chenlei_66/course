package com.course.server.service;

import com.course.server.domain.Test;

import java.util.List;

public interface TestService {

    List<Test> list();
}
