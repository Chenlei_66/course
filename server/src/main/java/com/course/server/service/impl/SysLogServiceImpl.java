package com.course.server.service.impl;

import com.course.server.domain.SysLog;
import com.course.server.domain.SysLogDetail;
import com.course.server.domain.SysLogExample;
import com.course.server.dto.SysLogDTO;
import com.course.server.dto.SysLogPageDTO;
import com.course.server.mapper.SysLogDetailMapper;
import com.course.server.mapper.SysLogMapper;
import com.course.server.service.SysLogService;
import com.course.server.util.CopyUtil;
import com.course.server.util.UuidUtil;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.List;
import java.util.Map;

/**
 * 操作日志业务接口实现层
 *
 * @author cl
 * @date 2020/9/18 16:09
 */
@Service
public class SysLogServiceImpl implements SysLogService {

    @Resource
    private SysLogMapper sysLogMapper;
    @Resource
    private SysLogDetailMapper sysLogDetailMapper;


    /**
     * 列表查询
     *
     * @param sysLogPageDTO 日志前端请求参数DTO
     */
    @Override
    public void list(SysLogPageDTO sysLogPageDTO) {
        PageHelper.startPage(sysLogPageDTO.getPage(),sysLogPageDTO.getSize());
        SysLogExample sysLogExample = new SysLogExample();
        SysLogExample.Criteria criteria = sysLogExample.createCriteria();
        criteria.andBusinessIdEqualTo(sysLogPageDTO.getBusinessId());
        criteria.andResultEqualTo(sysLogPageDTO.getResult());
        List<SysLog> sysLogList = sysLogMapper.selectByExample(sysLogExample);
        PageInfo<SysLog> pageInfo = new PageInfo<>(sysLogList);
        sysLogPageDTO.setTotal(pageInfo.getTotal());
        List<SysLogDTO> sectionDTOList = CopyUtil.copyList(sysLogList, SysLogDTO.class);
        sysLogPageDTO.setList(sectionDTOList);

    }

    /**
     * 保存操作日志
     *
     * @param ip          处理服务器ip
     * @param serverName  服务名
     * @param userNo      操作人
     * @param consumeTime 处理耗时
     * @param response    响应参数
     * @param result      处理结果
     * @param requestMap  请求map
     */
    @Override
    @Async
    public void saveLogs(String ip, String serverName, String userNo, long consumeTime, String response, String result, Map<String, Object> requestMap) {
        SysLog sysLog = new SysLog();
        SysLogDetail sysLogDetail = new SysLogDetail();

        String uuid = UuidUtil.getShortUuid();
        sysLog.setId(uuid);
        sysLog.setBusinessId(uuid);
        sysLog.setIp(ip);
        sysLog.setService(serverName);
        sysLog.setResult(result);
        sysLog.setUser(userNo);
        sysLog.setConsumeTime(consumeTime);
        sysLog.setMethod((String) requestMap.get("methodName"));
        sysLog.setDescription((String) requestMap.get("logRecordDesc"));

        // 数据库可存储响应的最大长度
        int dbMaxLength = 2500;
        if (response.length() > dbMaxLength) {
            response = response.substring(0, dbMaxLength);
        }
        String requestStr = requestMap.get("request") == null ? "" : requestMap.get("request").toString();
        if (requestStr.length() > dbMaxLength) {
            requestStr = requestStr.substring(0, dbMaxLength);
        }
        sysLogDetail.setId(uuid);
        sysLogDetail.setRequest(requestStr);
        sysLogDetail.setResponse(response);
        // 保存到数据库
        insertSysLogInfo(sysLog, sysLogDetail);
    }

    /**
     * 插入日志
     *
     * @param sysLog       操作信息
     * @param sysLogDetail 操作详情信息
     */
    @Override
    @Transactional
    public void insertSysLogInfo(SysLog sysLog, SysLogDetail sysLogDetail) {
        sysLogMapper.insertSelective(sysLog);
        sysLogDetailMapper.insertSelective(sysLogDetail);
    }
}
