package com.course.server.service;

import com.course.server.domain.SysLog;
import com.course.server.domain.SysLogDetail;
import com.course.server.dto.SysLogPageDTO;

import java.util.Map;

/**
 * 日志操作业务接口层
 * @author cl
 * @description
 * @date 2020/9/18 16:07
 */
public interface SysLogService {

    /**
     * 列表查询
     * @param sysLogPageDTO
     */
    void list(SysLogPageDTO sysLogPageDTO);

    /**
     * 保存操作日志
     * @param ip 处理服务器ip
     * @param serverName 服务名
     * @param userNo 操作人
     * @param consumeTime 处理耗时
     * @param response 响应参数
     * @param result 处理结果
     * @param requestMap 请求map
     */
    void saveLogs(String ip, String serverName, String userNo, long consumeTime, String response, String result, Map<String, Object> requestMap);


    /**
     * 插入日志
     * @param sysLog 操作信息
     * @param sysLogDetail 操作详情信息
     */
    void insertSysLogInfo(SysLog sysLog, SysLogDetail sysLogDetail);


}
