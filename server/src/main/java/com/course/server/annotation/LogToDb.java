package com.course.server.annotation;

import com.course.server.enums.OperationTypeEnum;

import java.lang.annotation.*;

/**
 * 自定义日志记录注解
 *
 * @author cl
 * @description 用于日志信息记录到数据库
 * @date 2020/9/18 15:40
 */
@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface LogToDb {
    OperationTypeEnum value();
}
