package com.course.server.enums;

/**
 * @description 小节收费类型枚举类
 * @author cl
 * @date 2020/7/28 12:23
 */
public enum SectionChargeEnum {

    CHARGE("C","收费"),
    FREE("F","免费");

    private String code;

    private String desc;

    SectionChargeEnum(String code, String desc) {
        this.code = code;
        this.desc = desc;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }
}
