package com.course.server.enums;

/**
 * 请求处理结果枚举类
 * @author cl
 * @description
 * @date 2020/9/21 9:44
 */
public enum OperationResultEnum {
    YES("1", "成功"),
    NO("2", "失败");

    private String code;

    private String desc;

    OperationResultEnum(String code, String desc) {
        this.code = code;
        this.desc = desc;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }
}
