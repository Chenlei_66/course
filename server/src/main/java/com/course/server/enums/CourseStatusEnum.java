package com.course.server.enums;

/**
 * @description 课程状态枚举类
 * @author cl
 * @date 2020/7/28 15:23
 */
public enum CourseStatusEnum {

    PUBLISH("P","发布"),
    DRAFT("D","草稿");

    private String code;

    private String desc;

    CourseStatusEnum(String code, String desc) {
        this.code = code;
        this.desc = desc;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }
}
