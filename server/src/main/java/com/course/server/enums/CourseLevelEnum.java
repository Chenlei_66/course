package com.course.server.enums;

/**
 * @description 课程级别枚举类
 * @author cl
 * @date 2020/7/28 15:23
 */
public enum CourseLevelEnum {

    ONE("1","初级"),
    TWO("2","中级"),
    THREE("3","高级");

    private String code;

    private String desc;

    CourseLevelEnum(String code, String desc) {
        this.code = code;
        this.desc = desc;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }
}
