package com.course.server.enums;

/**
 * @description 文件用途枚举类
 * @author cl
 * @date 2020/8/20 10:14
 */
public enum FileUseEnum {

    COURSE("C","课程"),
    TEACHER("T","讲师");

    private String code;

    private String desc;

    FileUseEnum(String code, String desc) {
        this.code = code;
        this.desc = desc;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    /**
     * 根据code获取枚举类型
     * @param code
     * @return
     */
    public static FileUseEnum getByCode(String code) {
        for(FileUseEnum e : FileUseEnum.values()) {
            if(e.code.equals(code)) {
                return e;
            }
        }
        return null;
    }
}
