package com.course.server.enums;

/**
 * 操作类型枚举类
 *
 * @author cl
 * @description
 * @date 2020/9/21 9:44
 */
public enum OperationTypeEnum {
    CATEGORY_LIST("分类-列表查询"),
    CATEGORY_QUERY("分类-条件查询"),
    CATEGORY_SAVE("分类-保存"),
    CATEGORY_DEL("分类-删除"),
    CHAPTER_LIST("大章-列表查询"),
    CHAPTER_QUERY("大章-条件查询"),
    CHAPTER_SAVE("大章-保存"),
    CHAPTER_DEL("大章-删除"),
    SECTION_LIST("小节-列表查询"),
    SECTION_QUERY("小节-条件查询"),
    SECTION_SAVE("小节-保存"),
    SECTION_DEL("小节-删除"),
    COURSE_LIST("课程-列表查询"),
    COURSE_QUERY("课程-条件查询"),
    COURSE_SAVE("课程-保存"),
    COURSE_DEL("课程-删除"),
    TEACHER_LIST("讲师-列表查询"),
    TEACHER_QUERY("讲师-条件查询"),
    TEACHER_SAVE("讲师-保存"),
    TEACHER_DEL("讲师-删除"),
    ;

    private String desc;

    OperationTypeEnum(String desc) {
        this.desc = desc;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }
}
