package com.course.server.enums;

/**
 * @Description
 * @Author cl
 * @Date 2020/7/28 12:43
 */
public enum YesNoEnum {
    YES("1", "是"),
    NO("2", "否");

    private String code;

    private String desc;

    YesNoEnum(String code, String desc) {
        this.code = code;
        this.desc = desc;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }
}
