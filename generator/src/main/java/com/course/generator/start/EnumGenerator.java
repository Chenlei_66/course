package com.course.generator.start;

import com.course.generator.util.FieldUtil;
import com.course.server.enums.*;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.lang.reflect.Method;
import java.nio.charset.StandardCharsets;

/**
 * @description 前端枚举类生成器
 * @author cl
 * @date 2020/7/28 12:57
 */
public class EnumGenerator {
    static String path = "admin\\public\\static\\js\\enums.js";

    public static void main(String[] args) {
        StringBuffer bufferObject = new StringBuffer();
        StringBuffer bufferArray = new StringBuffer();
        long begin = System.currentTimeMillis();

        try {
            toJson(SectionChargeEnum.class, bufferObject, bufferArray);
            toJson(YesNoEnum.class, bufferObject, bufferArray);
            toJson(CourseLevelEnum.class, bufferObject, bufferArray);
            toJson(CourseStatusEnum.class, bufferObject, bufferArray);
            toJson(CourseChargeEnum.class, bufferObject, bufferArray);
            toJson(FileUseEnum.class, bufferObject, bufferArray);

            StringBuffer buffer = bufferObject.append("\r\n").append(bufferArray);
            writeJs(buffer);
        } catch (Exception e) {
            e.printStackTrace();
        }
        long end = System.currentTimeMillis();
        System.out.println("执行耗时：" + (end - begin) + "毫秒");

    }


    /**
     * 转json字符串
     * @param clazz java类型
     * @param bufferObject 要转成的目标对象json
     * @param bufferArray 要转成的目标数组json
     * @throws Exception
     */
    private static void toJson(Class clazz, StringBuffer bufferObject, StringBuffer bufferArray) throws Exception {
        String key = FieldUtil.toUnderline(clazz.getSimpleName());
        toJson(clazz, key, bufferObject, bufferArray);

    }

    /**
     * 转json字符串
     * @param clazz java类型
     * @param key 对象名称
     * @param bufferObject 要转成的目标对象json
     * @param bufferArray 要转成的目标数组json
     * @throws Exception
     */
    private static void toJson(Class clazz, String key, StringBuffer bufferObject, StringBuffer bufferArray) throws Exception {
        Object[] objects = clazz.getEnumConstants();
        Method name = clazz.getMethod("name");
        Method getDesc = clazz.getMethod("getDesc");
        Method getCode = clazz.getMethod("getCode");

        // 生成对象
        bufferObject.append(key).append("={");
        for (int i = 0; i < objects.length; i++) {
            Object obj = objects[i];
            if (getCode == null) {
                bufferObject.append(name.invoke(obj)).append(":{key:\"").append(name.invoke(obj))
                        .append("\",value:\"").append(getDesc.invoke(obj)).append("\"}");
            } else {
                bufferObject.append(name.invoke(obj)).append(":{key:\"").append(getCode.invoke(obj))
                        .append("\",value:\"").append(getDesc.invoke(obj)).append("\"}");
            }
            if (i < objects.length - 1) {
                bufferObject.append(",");
            }
        }
        bufferObject.append("};\r\n");

        // 生成数组
        bufferArray.append(key).append("_ARRAY=[");
        for (int i = 0; i < objects.length; i++) {
            Object obj = objects[i];
            if (getCode == null) {
                bufferArray.append("{key:\"").append(name.invoke(obj))
                        .append("\",value:\"").append(getDesc.invoke(obj)).append("\"}");
            } else {
                bufferArray.append("{key:\"").append(getCode.invoke(obj))
                        .append("\",value:\"").append(getDesc.invoke(obj)).append("\"}");
            }
            if (i < objects.length - 1) {
                bufferArray.append(",");
            }
        }
        bufferArray.append("];\r\n");
    }

    /**
     * 写文件
     * @param stringBuffer 文件字节流
     */
    private static void writeJs(StringBuffer stringBuffer) {
        FileOutputStream out = null;
        try {
            out = new FileOutputStream(path);
            OutputStreamWriter osw = new OutputStreamWriter(out, StandardCharsets.UTF_8);
            System.out.println(path);
            osw.write(stringBuffer.toString());
            osw.close();
        }catch (Exception e){
            e.printStackTrace();
        }finally {
            try {
                if (out != null) {
                    out.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
