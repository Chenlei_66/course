package com.course.generator.start;

import com.course.generator.util.DbUtil;
import com.course.generator.util.Field;
import com.course.generator.util.FieldUtil;
import com.course.generator.util.FreemarkerUtil;
import org.dom4j.Document;
import org.dom4j.Element;
import org.dom4j.io.SAXReader;

import java.io.File;
import java.util.*;

/**
 * @description 前端代码生成器
 * @author cl
 * @date 2020/7/28 12:57
 */
public class VueGenerator {
    static String MODULE = "system";
    static String toVuePath = "admin\\src\\views\\admin\\";
    static String generatorConfigPath = "server\\src\\main\\resources\\generator\\generatorConfig.xml";

    public static void main(String[] args) throws Exception {
        String module = MODULE;

        File file = new File(generatorConfigPath);
        SAXReader reader = new SAXReader();
        // 读取xml文件到Document中
        Document  doc = reader.read(file);
        // 获取xml文件的根节点
        Element rootElement = doc.getRootElement();
        // 读取context节点
        Element  contextElement = rootElement.element("context");
        // 定义一个element用于遍历
        Element tableElement;
        // 取第一个“table”的节点
        tableElement = contextElement.elementIterator("table").next();
        String Domain = tableElement.attributeValue("domainObjectName");
        String tableName = tableElement.attributeValue("tableName");
        String tableNameCn = DbUtil.getTableComment(tableName);
        String domain = Domain.substring(0,1).toLowerCase() + Domain.substring(1);
        System.out.println("表：" + tableElement.attributeValue("tableName"));
        System.out.println("Domain：" + tableElement.attributeValue("domainObjectName"));

        List<Field> fieldList = DbUtil.getColumnByTableName(tableName);
        Set<String> typeSet = FieldUtil.getJavaTypes(fieldList);
        Map<String, Object> map = new HashMap<>();
        map.put("Domain", Domain);
        map.put("domain", domain);
        map.put("tableNameCn", tableNameCn);
        map.put("module", module);
        map.put("fieldList", fieldList);
        map.put("typeSet", typeSet);

        // 生成vue
        FreemarkerUtil.initConfig("vue.ftl");
        FreemarkerUtil.generator(toVuePath + domain + ".vue",map);

    }
}
