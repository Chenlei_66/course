package com.course.generator.util;

import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @Description
 * @Author cl
 * @Date 2020/7/27 9:45
 */
public class FieldUtil {

    /**
     * 获取所有的Java类型，使用Set去重
     * @param fieldList 表字段描述集合
     * @return 去重后的表字段列表
     */
    public static Set<String> getJavaTypes(List<Field> fieldList) {
        Set<String> set = new HashSet<>();
        for (Field field : fieldList) {
            set.add(field.getJavaType());
        }
        return set;
    }

    /**
     * 驼峰转大写下划线，并去掉_ENUM
     * 如：SectionChargeEnum 变为SECTION_CHARGE
     *
     * @param str
     * @return
     */
    public static String toUnderline(String str) {
        String result = underLine(str).toString();
        return result.substring(1, result.length()).toUpperCase().replace("_ENUM", "");
    }

    /**
     * 驼峰转下划线，第一位是下划线
     * 如：SectionChargeEnum 变成 _section_charge_enum
     *
     * @param str
     * @return
     */
    public static StringBuffer underLine(String str) {
        Pattern pattern = Pattern.compile("[A-Z]");
        Matcher matcher = pattern.matcher(str);
        StringBuffer sb = new StringBuffer(str);
        if (matcher.find()) {
            sb = new StringBuffer();
            matcher.appendReplacement(sb, "_" + matcher.group(0).toLowerCase());
            matcher.appendTail(sb);
        } else {
            return sb;
        }
        return underLine(sb.toString());
    }
}
