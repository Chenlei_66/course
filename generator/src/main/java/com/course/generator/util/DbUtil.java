package com.course.generator.util;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class DbUtil {

    /**
     * 获取数据库连接
     *
     * @return
     */
    public static Connection getConnection() {
        Connection conn = null;
        try {
            Class.forName("com.mysql.jdbc.Driver");
            String url = "jdbc:mysql://127.0.0.1:3306/course?useUnicode=true&characterEncoding=utf-8&useSSL=false&serverTimezone=GMT";
            String user = "course";
            String pass = "123456";
            conn = DriverManager.getConnection(url, user, pass);
        } catch (ClassNotFoundException | SQLException e) {
            e.printStackTrace();
        }
        return conn;
    }

    /**
     * 获取表注释
     *
     * @param tableName
     * @return
     */
    public static String getTableComment(String tableName) throws SQLException {
        Connection conn = getConnection();
        Statement statement = conn.createStatement();
        ResultSet rs = statement.executeQuery("select table_comment from information_schema.TABLES where TABLE_NAME = '" + tableName + "'");

        String tableNameCH = "";
        if (rs != null) {
            while (rs.next()) {
                tableNameCH = rs.getString("table_comment");
                break;
            }
        }
        rs.close();
        statement.close();
        conn.close();
        System.out.println("表名：" + tableNameCH);
        return tableNameCH;
    }

    /**
     * 根据表名获取该表所有字段属性集合
     * @param tableName
     * @return
     * @throws SQLException
     */
    public static List<Field> getColumnByTableName(String tableName) throws SQLException {
        List<Field> fieldList = new ArrayList<>();
        Connection conn = getConnection();
        Statement statement = conn.createStatement();
        ResultSet rs = statement.executeQuery("show full columns from `" + tableName + "`");
        if(rs != null){
            while (rs.next()) {
                String columnName = rs.getString("Field");
                String type = rs.getString("Type");
                String comment = rs.getString("Comment");
                String nullAble = rs.getString("Null");
                String fieldDefault = rs.getString("Default");
                String fieldName = rs.getString("Field");
                Field field = new Field();
                field.setName(columnName);
                field.setNameHump(lineToHump(columnName));
                field.setNameBigHump(lineToBigHump(columnName));
                field.setType(type);
                field.setJavaType(sqlTypeToJavaType(type));
                field.setComment(comment);
                if(comment.contains("|")){
                    field.setNameCn(comment.substring(0,comment.indexOf("|")));
                }else {
                    field.setNameCn(comment);
                }
                // 已设置默认值字段 & id字段不参与校验
                if(fieldDefault == null && !"id".equals(fieldName)){
                    field.setNullAble("YES".equals(nullAble));
                }else {
                    field.setNullAble(true);
                }
                if(!field.getNullAble()
                        && type.toUpperCase().contains("varchar".toUpperCase())){
                    String lengthStr = type.substring(type.indexOf("(") + 1, type.length() - 1);
                    field.setLength(Integer.valueOf(lengthStr));
                }else {
                    field.setLength(0);
                }
                // 枚举字段读取
                if(comment.contains("枚举")){
                    field.setEnums(true);

                    int start = comment.indexOf("[");
                    int end = comment.indexOf("]");
                    String enumsName = comment.substring(start + 1, end);
                    String enumsConst = FieldUtil.toUnderline(enumsName);
                    field.setEnumsConst(enumsConst);
                }else {
                    field.setEnums(false);
                }
                fieldList.add(field);
            }
        }
        rs.close();
        statement.close();
        conn.close();
        System.out.println("列信息：" + fieldList);
        return fieldList;
    }

    /**
     * 下划线转小驼峰
     * @param str
     * @return
     */
    public static String lineToHump(String str){
        Pattern linePattern = Pattern.compile("_(\\w)");
        str = str.toLowerCase();
        Matcher matcher = linePattern.matcher(str);
        StringBuffer sb = new StringBuffer();
        while (matcher.find()) {
            matcher.appendReplacement(sb, matcher.group(1).toUpperCase());
        }
        matcher.appendTail(sb);
        return sb.toString();
    }

    /**
     * 下划线转大驼峰
     * @param str
     * @return
     */
    public static String lineToBigHump(String str){
        String s = lineToHump(str);
        return s.substring(0,1).toUpperCase() + s.substring(1);
    }

    public static String sqlTypeToJavaType(String sqlType){
        if(sqlType.toUpperCase().contains("varchar".toUpperCase())
                || sqlType.toUpperCase().contains("char".toUpperCase())
                || sqlType.toUpperCase().contains("text".toUpperCase())){
            return "String";
        }else if (sqlType.toUpperCase().contains("dateTime".toUpperCase())
                || sqlType.toUpperCase().contains("timestamp".toUpperCase())){
            return "Date";
        }else if (sqlType.toUpperCase().contains("int".toUpperCase())){
            return "Integer";
        }else if(sqlType.toUpperCase().contains("long".toUpperCase())){
            return "Long";
        }else if(sqlType.toUpperCase().contains("decimal".toUpperCase())){
            return "BigDecimal";
        }else {
            return "String";
        }
    }



}
