package com.course.generator.util;

import freemarker.template.Configuration;
import freemarker.template.DefaultObjectWrapper;
import freemarker.template.Template;
import freemarker.template.TemplateException;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Map;

/**
 * FreeMarker连接工具类
 */
public class FreemarkerUtil {
    static String ftlPath = "generator\\src\\main\\java\\com\\course\\generator\\ftl\\";

    static Template temp;

    /**
     * 初始化配置
     * @param ftlName
     * @throws IOException
     */
    public static void initConfig(String ftlName) throws IOException {
        Configuration cfg = new Configuration(Configuration.VERSION_2_3_30);
        cfg.setDirectoryForTemplateLoading(new File(ftlPath));
        cfg.setObjectWrapper(new DefaultObjectWrapper(Configuration.VERSION_2_3_30));
        temp = cfg.getTemplate(ftlName);
    }

    /**
     * 模板构建
     * @param fileName
     * @param map
     * @throws IOException
     * @throws TemplateException
     */
    public static void generator(String fileName, Map<String,Object> map) throws IOException, TemplateException {
        FileWriter fw = new FileWriter(fileName);
        BufferedWriter bw = new BufferedWriter(fw);
        temp.process(map, bw);
        bw.flush();
        fw.close();
    }
}
