package com.course.generator.util;

/**
 * 类属性描述
 */
public class Field {
    /**
     * 字段名
     * course_id
     */
    private String name;
    /**
     * 字段名小驼峰
     * 例：courseId
     */
    private String nameHump;
    /**
     * 字段名大驼峰
     * 例：CourseId
     */
    private String nameBigHump;
    /**
     * 中文名
     * 例：课程
     */
    private String nameCn;
    /**
     * 字段类型
     * 例：char(1)
     */
    private String type;
    /**
     * java类型
     * 例：String
     */
    private String javaType;
    /**
     * 注释
     * 例：课程|ID
     */
    private String comment;

    /**
     * 是否可空
     */
    private Boolean nullAble;
    /**
     * 字符串长度
     * 非字符串该值约定为0
     */
    private Integer length;
    /**
     * 是否枚举类
     */
    private Boolean enums;
    /**
     * 枚举类常量
     */
    private String enumsConst;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getNameHump() {
        return nameHump;
    }

    public void setNameHump(String nameHump) {
        this.nameHump = nameHump;
    }

    public String getNameBigHump() {
        return nameBigHump;
    }

    public void setNameBigHump(String nameBigHump) {
        this.nameBigHump = nameBigHump;
    }

    public String getNameCn() {
        return nameCn;
    }

    public void setNameCn(String nameCn) {
        this.nameCn = nameCn;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getJavaType() {
        return javaType;
    }

    public void setJavaType(String javaType) {
        this.javaType = javaType;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public Boolean getNullAble() {
        return nullAble;
    }

    public void setNullAble(Boolean nullAble) {
        this.nullAble = nullAble;
    }

    public Integer getLength() {
        return length;
    }

    public void setLength(Integer length) {
        this.length = length;
    }

    public Boolean getEnums() {
        return enums;
    }

    public void setEnums(Boolean enums) {
        this.enums = enums;
    }

    public String getEnumsConst() {
        return enumsConst;
    }

    public void setEnumsConst(String enumsConst) {
        this.enumsConst = enumsConst;
    }

    @Override
    public String toString() {
        return "Field{" +
                "name='" + name + '\'' +
                ", nameHump='" + nameHump + '\'' +
                ", nameBigHump='" + nameBigHump + '\'' +
                ", nameCn='" + nameCn + '\'' +
                ", type='" + type + '\'' +
                ", javaType='" + javaType + '\'' +
                ", comment='" + comment + '\'' +
                ", nullAble=" + nullAble +
                ", length=" + length +
                ", enums=" + enums +
                ", enumsConst='" + enumsConst + '\'' +
                '}';
    }
}
