package com.course.server.service;

import com.course.server.dto.${Domain}DTO;
import com.course.server.dto.PageDTO;

public interface ${Domain}Service {
    /**
     * 列表查询
     * @param pageDTO
     */
    void list(PageDTO<${Domain}DTO> pageDTO);

    /**
     * 保存，id有值时更新，无值时新增
     * @param ${domain}DTO
     * @return
     */
    boolean save(${Domain}DTO ${domain}DTO);

    /**
     * 删除
     * @param id
     * @return
     */
    boolean delete(String id);
}
