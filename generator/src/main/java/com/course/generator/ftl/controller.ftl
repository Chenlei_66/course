package com.course.${module}.controller.admin;

import com.course.server.dto.${Domain}DTO;
import com.course.server.dto.PageDTO;
import com.course.server.dto.ResponseDTO;
import com.course.server.service.${Domain}Service;
import com.course.server.util.ValidatorUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;

@RestController
@RequestMapping("admin/${domain}")
public class ${Domain}Controller {

    public static final String BUSINESS_NAME = "${tableNameCn}";

    @Resource
    private ${Domain}Service ${domain}Service;

    /**
     * 列表查询
     * @param pageDTO
     * @return
     */
    @RequestMapping(value = "/list", method = RequestMethod.POST, produces = {"application/json;charset=UTF-8"})
    public ResponseDTO list(@RequestBody PageDTO<${Domain}DTO> pageDTO) {
        ResponseDTO<PageDTO<${Domain}DTO>> responseDTO = new ResponseDTO<>();
        ${domain}Service.list(pageDTO);
        responseDTO.setContent(pageDTO);
        return responseDTO;
    }

    /**
     * 保存，id有值时更新，无值时新增
     * @param ${domain}DTO
     * @return
     */
    @RequestMapping(value = "/save", method = RequestMethod.POST, produces = {"application/json;charset=UTF-8"})
    public ResponseDTO save(@RequestBody ${Domain}DTO ${domain}DTO) {
        // 保存校验
        <#list fieldList as field>
            <#if !field.nullAble>
        ValidatorUtil.require(${domain}DTO.get${field.nameBigHump}(),"${field.nameCn}");
            </#if>
            <#if (field.length > 0)>
        ValidatorUtil.length(${domain}DTO.get${field.nameBigHump}(),"${field.nameCn}",1,${field.length?c});
            </#if>
        </#list>

        ResponseDTO<${Domain}DTO> responseDTO = new ResponseDTO<>();
        boolean success = ${domain}Service.save(${domain}DTO);
        responseDTO.setSuccess(success);
        if(success){
            responseDTO.setContent(${domain}DTO);
        }
        return responseDTO;
    }

    /**
     * 删除
     * @param id
     * @return
     */
    @DeleteMapping(value = "/delete/{id}", produces = {"application/json;charset=UTF-8"})
    public ResponseDTO delete(@PathVariable String id) {
        ResponseDTO<${Domain}DTO> responseDTO = new ResponseDTO<>();
        boolean success = ${domain}Service.delete(id);
        responseDTO.setSuccess(success);
        return responseDTO;
    }
}
