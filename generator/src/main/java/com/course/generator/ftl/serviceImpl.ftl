package com.course.server.service.impl;

import com.course.server.domain.${Domain};
import com.course.server.domain.${Domain}Example;
import com.course.server.dto.${Domain}DTO;
import com.course.server.dto.PageDTO;
import com.course.server.mapper.${Domain}Mapper;
import com.course.server.service.${Domain}Service;
import com.course.server.util.CopyUtil;
import com.course.server.util.UuidUtil;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import javax.annotation.Resource;
import java.util.List;

@Service
public class ${Domain}ServiceImpl implements ${Domain}Service {
    @Resource
    private ${Domain}Mapper ${domain}Mapper;

    /**
     * 列表查询
     * @param pageDTO
     */
    @Override
    public void list(PageDTO<${Domain}DTO> pageDTO){
        PageHelper.startPage(pageDTO.getPage(),pageDTO.getSize());
        ${Domain}Example ${domain}Example = new ${Domain}Example();
        <#list fieldList as field>
            <#if field.nameHump == 'sort'>
        ${domain}Example.setOrderByClause("sort asc");
            </#if>
        </#list>


        List<${Domain}> ${domain}List = ${domain}Mapper.selectByExample(${domain}Example);
        PageInfo<${Domain}> pageInfo = new PageInfo<>(${domain}List);
        pageDTO.setTotal(pageInfo.getTotal());
        List<${Domain}DTO> ${domain}DTOList = CopyUtil.copyList(${domain}List, ${Domain}DTO.class);
        pageDTO.setList(${domain}DTOList);
    }

    /**
     * 保存，id有值时更新，无值时新增
     * @param ${domain}DTO
     * @return
     */
    @Override
    public boolean save(${Domain}DTO ${domain}DTO) {
        ${Domain} ${domain} = CopyUtil.copy(${domain}DTO, ${Domain}.class);
        boolean result;
        if(StringUtils.isEmpty(${domain}DTO.getId())){
            result = insert(${domain});
        }else {
            result = update(${domain});
        }
        return result;
    }

    /**
     * 删除
     * @param id
     * @return
     */
    @Override
    public boolean delete(String id) {
        return ${domain}Mapper.deleteByPrimaryKey(id) == 1;
    }

    private boolean insert(${Domain} ${domain}) {
        ${domain}.setId(UuidUtil.getShortUuid());
        return ${domain}Mapper.insert(${domain}) == 1;
    }

    private boolean update(${Domain} ${domain}) {
        return ${domain}Mapper.updateByPrimaryKey(${domain}) == 1;
    }
}
