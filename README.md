## 项目名称:在线网课

## 项目概述：

## 开发环境：
    【罗列使用本工程项目所需要安装的开发环境及配置】
     mvn install:install-file -Dfile=aliyun-java-sdk-vod-2.15.11.jar  -DgroupId=com.aliyun  -DartifactId=aliyun-java-sdk-vod -Dversion=2.15.11 -Dpackaging=jar

### 基础服务启动顺序：
- 启动|eureka(参考|eureka[README.md](|eureka/readme.md))
- 启动|gateway(参考|gateway[README.md](|gateway/readme.md))
- 启动|system(参考|system[README.md](|system/readme.md))
- 启动|business(参考|business下的[README.md](|business/readme.md))
- 启动|file(参考|file下的[README.md](|file/readme.md))
- 启动admin(参考|admin下的[README.md](|aos-web/readme.md))
- 启动web(参考|web下的[README.md](|aos-web/readme.md))
- 启动redis(redis-server start)
- 浏览器访问： http://localhost:8080/
- 登陆账号密码：test/123

## 历史版本
| 序号 | 版本号 | 发版日期 | 更新内容 |
| :------: | :------: | :------: | :------ |
| 1 | V1.0.0 | 20201011 | 初始版本 |

## 系统模块：
| 类型 | 模块名 | 模块描述 |
| :------: | :------: | :------ |
| 架构 | eureka | 服务治理中心 |
| 架构 | gateway | 网关路由 |
| 服务 | generator | 代码生成工具模块，用于自动生成单表前后端代码 |
| 服务 | system | 系统管理模块，用户登录及权限 |
| 服务 | business | 业务模块，处理业务逻辑 |
| 服务 | file | 文件模块,文件上传下载及记录 |
| 服务 | admin | 管理控台-前端 |
| 服务 | web | 客户端-前端 |
| 公共包 | server | 公共依赖包server |
| 文件夹 | doc | 数据库表结构 & 数据初始化 & 项目依赖的jar包 |
