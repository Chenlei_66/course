# 测试表
DROP TABLE IF EXISTS `test`;
CREATE TABLE `test`  (
    `id` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
    `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
    PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

insert into `test` values ('1', 'test');
insert into `test` values ('2', 'test2');

# 大章表
DROP TABLE IF EXISTS `chapter`;
CREATE TABLE `chapter`  (
  `id` varchar(32)  NOT NULL comment 'id',
  `name` varchar(50) comment '名称',
  `course_id` varchar(32) comment '课程id',
  `create_user` varchar(50) comment '创建人',
  `create_time` timestamp default CURRENT_TIMESTAMP comment '创建时间',
  `update_user` varchar(50) comment '修改人',
  `update_time` timestamp  default CURRENT_TIMESTAMP comment '修改时间',

  PRIMARY KEY (`id`)
) ENGINE = InnoDB CHARACTER SET = utf8mb4 comment '大章';

insert into `chapter`(id, name, course_id, create_user, update_user) values ('00000000','测试大章00','00000000','admin','admin');
insert into `chapter`(id, name, course_id, create_user, update_user) values ('00000001','测试大章01','00000000','admin','admin');
insert into `chapter`(id, name, course_id, create_user, update_user) values ('00000003','测试大章03','00000000','admin','admin');
insert into `chapter`(id, name, course_id, create_user, update_user) values ('00000004','测试大章04','00000000','admin','admin');
insert into `chapter`(id, name, course_id, create_user, update_user) values ('00000005','测试大章05','00000000','admin','admin');
insert into `chapter`(id, name, course_id, create_user, update_user) values ('00000006','测试大章06','00000000','admin','admin');
insert into `chapter`(id, name, course_id, create_user, update_user) values ('00000007','测试大章07','00000000','admin','admin');
insert into `chapter`(id, name, course_id, create_user, update_user) values ('00000008','测试大章08','00000000','admin','admin');

# 小节表
DROP TABLE IF EXISTS `section`;
CREATE TABLE `section`  (
    `id` varchar(32)  NOT NULL comment 'id',
    `title` varchar(50)  NOT NULL comment '标题',
    `course_id` varchar(32) comment '课程|course.id',
    `chapter_id` varchar(32) comment '大章|chapter.id',
    `video` varchar(200) comment '视频',
    `time` INT comment '时长|单位秒',
    `charge` CHAR(1) comment '收费|CHARGE("C","收费"),FREE("F","免费")',
    `sort` INT comment '顺序',
    `create_user` varchar(50) comment '创建人',
    `create_time` timestamp default CURRENT_TIMESTAMP comment '创建时间',
    `update_user` varchar(50) comment '修改人',
    `update_time` timestamp  default CURRENT_TIMESTAMP comment '修改时间',
    PRIMARY KEY (`id`)
) ENGINE = InnoDB CHARACTER SET = utf8mb4 comment '小节';

insert into `section`(id, title, course_id, chapter_id, video, time, charge, sort, create_user, update_user) values('00000001','测试小节01','00000000','00000000','',500,'F',1,'admin','admin');


# 课程表
DROP TABLE IF EXISTS `course`;
CREATE TABLE `course`  (
    `id` varchar(32)  NOT NULL comment 'id',
    `name` varchar(50)  NOT NULL comment '名称',
    `summary` varchar(2000) comment '概述',
    `time` INT comment '时长|单位秒',
    `price` decimal(8,2) default 0.00 comment '价格（元）',
    `image` varchar(2500) comment '封面',
    `level` char(1) comment '级别|枚举[CourseLevelEnum]：ONE("1","初级"),TWO("2","中级"),THREE("3","高级")',
    `charge` char(1) comment '收费|枚举[CourseChargeEnum]：CHARGE("C","收费"),FREE("F","免费")',
    `status` char(1) comment '状态|枚举[CourseStatusEnum]：PUBLISH("P","发布"),TWO("2","中级"),DRAFT("D","草稿")',
    `enroll` integer default 0 comment '报名数',
    `sort` int comment '顺序',
    `create_user` varchar(50) comment '创建人',
    `create_time` timestamp default CURRENT_TIMESTAMP comment '创建时间',
    `update_user` varchar(50) comment '修改人',
    `update_time` timestamp  default CURRENT_TIMESTAMP comment '修改时间',
    PRIMARY KEY (`id`)
) ENGINE = InnoDB CHARACTER SET = utf8mb4 comment '课程';

insert into `course` (id, name, summary, `time`, price, image, level, charge, status, sort)
value ('00000001','测试课程01','这是一门测试课程',72000,19.9,'',0,'c','d',0);

-- 课程表增加讲师字段
alter table `course` add column (`teacher_id` varchar(32) comment '讲师|teacher.id');

# 分类
drop table if exists `category`;
create table `category` (
    `id` varchar(32)  NOT NULL comment 'id',
    `parent` varchar(32)  comment '父id',
    `name` varchar(50) comment '名称',
    `sort` int comment '顺序',
    `create_user` varchar(50) comment '创建人',
    `create_time` timestamp default CURRENT_TIMESTAMP comment '创建时间',
    `update_user` varchar(50) comment '修改人',
    `update_time` timestamp  default CURRENT_TIMESTAMP comment '修改时间',
    PRIMARY KEY (`id`)
) ENGINE = InnoDB CHARACTER SET = utf8mb4 comment '分类';

insert into `category`(id,parent,name,sort) values ('00000100','00000000','前端技术',100);
insert into `category`(id,parent,name,sort) values ('00000101','00000100','html/cs',101);
insert into `category`(id,parent,name,sort) values ('00000102','00000100','javascript',102);
insert into `category`(id,parent,name,sort) values ('00000103','00000100','vue.js',103);
insert into `category`(id,parent,name,sort) values ('00000104','00000100','react.js',104);
insert into `category`(id,parent,name,sort) values ('00000105','00000100','angular',105);
insert into `category`(id,parent,name,sort) values ('00000106','00000100','node.js',106);
insert into `category`(id,parent,name,sort) values ('00000107','00000100','jquery',107);
insert into `category`(id,parent,name,sort) values ('00000200','00000000','后端技术',200);
insert into `category`(id,parent,name,sort) values ('00000201','00000200','java',201);
insert into `category`(id,parent,name,sort) values ('00000202','00000200','springboot',202);
insert into `category`(id,parent,name,sort) values ('00000203','00000200','spring cloud',203);
insert into `category`(id,parent,name,sort) values ('00000204','00000200','ssm',204);
insert into `category`(id,parent,name,sort) values ('00000205','00000200','python',205);
insert into `category`(id,parent,name,sort) values ('00000206','00000200','爬虫',206);
insert into `category`(id,parent,name,sort) values ('00000300','00000000','移动开发',300);
insert into `category`(id,parent,name,sort) values ('00000301','00000300','android',301);
insert into `category`(id,parent,name,sort) values ('00000302','00000300','ios',302);
insert into `category`(id,parent,name,sort) values ('00000303','00000300','react native',303);
insert into `category`(id,parent,name,sort) values ('00000304','00000300','ionic',304);
insert into `category`(id,parent,name,sort) values ('00000400','00000000','前沿技术',400);
insert into `category`(id,parent,name,sort) values ('00000401','00000400','微服务',401);
insert into `category`(id,parent,name,sort) values ('00000402','00000400','区块链',402);
insert into `category`(id,parent,name,sort) values ('00000403','00000400','数据分析&挖掘',403);
insert into `category`(id,parent,name,sort) values ('00000404','00000400','云计算&大数据',404);

# 课程分类
drop table if exists `course_category`;
create table `course_category` (
    `id` varchar(32)  NOT NULL comment 'id',
    `course_id` varchar(32)  comment '课程|course.id',
    `category_id` varchar(32) comment '分类|category.id',
    `create_user` varchar(50) comment '创建人',
    `create_time` timestamp default CURRENT_TIMESTAMP comment '创建时间',
    `update_user` varchar(50) comment '修改人',
    `update_time` timestamp  default CURRENT_TIMESTAMP comment '修改时间',
    PRIMARY KEY (`id`)
) ENGINE = InnoDB CHARACTER SET = utf8mb4 comment '课程分类';


# 课程内容
drop table if exists `course_content`;
create table `course_content` (
    `id` varchar(32)  NOT NULL comment '课程id',
    `content` mediumtext not null comment '课程内容',
    `create_user` varchar(50) comment '创建人',
    `create_time` timestamp default CURRENT_TIMESTAMP comment '创建时间',
    `update_user` varchar(50) comment '修改人',
    `update_time` timestamp  default CURRENT_TIMESTAMP comment '修改时间',
    PRIMARY KEY (`id`)
) ENGINE = InnoDB CHARACTER SET = utf8mb4 comment '课程内容';

# 课程内容文件
drop table if exists `course_content_file`;
create table `course_content_file` (
    `id` varchar(32)  NOT NULL comment 'id',
    `course_id` varchar(32) not null comment '课程id',
    `url` varchar(100) comment '地址',
    `name` varchar(100) comment '文件名',
    `size` int comment '大小|字节b',
    `create_user` varchar(50) comment '创建人',
    `create_time` timestamp default CURRENT_TIMESTAMP comment '创建时间',
    `update_user` varchar(50) comment '修改人',
    `update_time` timestamp  default CURRENT_TIMESTAMP comment '修改时间',
    PRIMARY KEY (`id`)
) ENGINE = InnoDB CHARACTER SET = utf8mb4 comment '课程内容文件';

# 讲师
drop table if exists `teacher`;
create table `teacher` (
    `id` varchar(32)  NOT NULL comment 'id',
    `name` varchar(50) not null comment '姓名',
    `nick_name` varchar(50) comment '昵称',
    `image` varchar(100) comment '头像',
    `position` varchar(50) comment '职位',
    `motto` varchar(50) comment '座右铭',
    `intro` varchar(50) comment '简介',
    `create_user` varchar(50) comment '创建人',
    `create_time` timestamp default CURRENT_TIMESTAMP comment '创建时间',
    `update_user` varchar(50) comment '修改人',
    `update_time` timestamp  default CURRENT_TIMESTAMP comment '修改时间',
    PRIMARY KEY (`id`)
) ENGINE = InnoDB CHARACTER SET = utf8mb4 comment '讲师';

# 文件
drop table if exists `file`;
create table `file` (
    `id` varchar(32)  NOT NULL comment 'id',
    `path` varchar(100) not null comment '相对路径',
    `name` varchar(100) comment '文件名',
    `suffix` varchar(10) comment '后缀',
    `size` int comment '大小|字节B',
    `use` char(1) comment '用途|枚举[FileUseEnum]：COURSE("C","课程"),TEACHER("T","讲师")',
    `create_user` varchar(50) comment '创建人',
    `create_time` timestamp default CURRENT_TIMESTAMP comment '创建时间',
    `update_user` varchar(50) comment '修改人',
    `update_time` timestamp  default CURRENT_TIMESTAMP comment '修改时间',
    PRIMARY KEY (`id`),
    unique key `path_unique` (`path`)
) ENGINE = InnoDB CHARACTER SET = utf8mb4 comment '文件';

alter table `file` add column (`shard_index` int comment '已上传分片');
alter table `file` add column (`shard_size` int comment '分片大小|B');
alter table `file` add column (`shard_total` int comment '分片总数');
alter table `file` add column (`key` varchar(32) comment '文件标识');
alter table `file` add unique key key_unique(`key`);

# 用户
drop table if exists `user`;
create table `user` (
        `id` varchar(32)  NOT NULL comment 'id',
        `login_name` varchar(50) not null comment '登录名',
        `name` varchar(50) comment '昵称',
        `password` varchar(32) not null comment '密码',
        `create_user` varchar(50) comment '创建人',
        `create_time` timestamp default CURRENT_TIMESTAMP comment '创建时间',
        `update_user` varchar(50) comment '修改人',
        `update_time` timestamp  default CURRENT_TIMESTAMP comment '修改时间',
        PRIMARY KEY (`id`),
        unique key `login_name_unique` (`login_name`)
) ENGINE = InnoDB CHARACTER SET = utf8mb4 comment '用户';

insert into `user`(id, login_name, name, password) values('10000000','test','测试','202cb96');

# 资源
drop table if exists `resource`;
create table `resource` (
    `id` varchar(32)  NOT NULL comment 'id',
    `name` varchar(100) comment '名称|菜单或按钮',
    `page` varchar(50) comment '页面|路由',
    `request` varchar(200) comment '请求|接口',
    `parent` varchar(32) comment '父id',
    `create_user` varchar(50) default 'system' comment '创建人',
    `create_time` timestamp default CURRENT_TIMESTAMP comment '创建时间',
    `update_user` varchar(50) default 'system' comment '修改人',
    `update_time` timestamp  default CURRENT_TIMESTAMP comment '修改时间',
    PRIMARY KEY (`id`)
) ENGINE = InnoDB CHARACTER SET = utf8mb4 comment '资源';

insert into `resource`(id,name,page,request,parent) values('01','系统管理',null,null,null);
insert into `resource`(id,name,page,request,parent) values('0101','用户管理','system/user',null,'01');
insert into `resource`(id,name,page,request,parent) values('010101','保存',null,'["/system/admin/user/list","/system/admin/user/save"]','0101');
insert into `resource`(id,name,page,request,parent) values('010102','删除',null,'["/system/admin/user/delete"]','0101');
insert into `resource`(id,name,page,request,parent) values('010103','重置密码',null,'["/system/admin/user/savePassword"]','0101');
insert into `resource`(id,name,page,request,parent) values('0102','资源管理','system/resource',null,'01');
insert into `resource`(id,name,page,request,parent) values('010201','保存/显示',null,'["/system/admin/resource"]','0102');
insert into `resource`(id,name,page,request,parent) values('0103','角色管理','system/role',null,'01');
insert into `resource`(id,name,page,request,parent) values('010301','角色/权限管理',null,'["/system/admin/role"]','0103');

# 角色
drop table if exists `role`;
create table `role` (
    `id` varchar(32)  NOT NULL comment 'id',
    `name` varchar(100) comment '角色',
    `desc` varchar(50) comment '描述',
    `create_user` varchar(50) default 'system' comment '创建人',
    `create_time` timestamp default CURRENT_TIMESTAMP comment '创建时间',
    `update_user` varchar(50) default 'system' comment '修改人',
    `update_time` timestamp  default CURRENT_TIMESTAMP comment '修改时间',
    PRIMARY KEY (`id`)
) ENGINE = InnoDB CHARACTER SET = utf8mb4 comment '角色';

insert into `role`(id,name,`desc`) values('00000000','系统管理员','管理用户、角色权限');
insert into `role`(id,name,`desc`) values('00000001','开发','维护资源');
insert into `role`(id,name,`desc`) values('00000002','业务管理员','负责业务管理');

# 角色资源
drop table if exists `role_resource`;
create table `role_resource` (
    `id` varchar(32)  NOT NULL comment 'id',
    `role_id` varchar(32) comment '角色|id',
    `resource_id` varchar(32) comment '资源|id',
    `create_user` varchar(50) default 'system' comment '创建人',
    `create_time` timestamp default CURRENT_TIMESTAMP comment '创建时间',
    `update_user` varchar(50) default 'system' comment '修改人',
    `update_time` timestamp  default CURRENT_TIMESTAMP comment '修改时间',
    PRIMARY KEY (`id`)
) ENGINE = InnoDB CHARACTER SET = utf8mb4 comment '角色资源';

insert into `role_resource`(id,role_id,resource_id) values('00000000','00000000','01');
insert into `role_resource`(id,role_id,resource_id) values('00000001','00000000','0101');
insert into `role_resource`(id,role_id,resource_id) values('00000002','00000000','010101');
insert into `role_resource`(id,role_id,resource_id) values('00000003','00000000','010102');
insert into `role_resource`(id,role_id,resource_id) values('00000004','00000000','010103');
insert into `role_resource`(id,role_id,resource_id) values('00000005','00000000','0102');
insert into `role_resource`(id,role_id,resource_id) values('00000006','00000000','010201');
insert into `role_resource`(id,role_id,resource_id) values('00000007','00000000','0103');
insert into `role_resource`(id,role_id,resource_id) values('00000008','00000000','010301');

# 用户角色
drop table if exists `user_role`;
create table `user_role` (
    `id` varchar(32)  NOT NULL comment 'id',
    `user_id` varchar(32) comment '用户|id',
    `role_id` varchar(32) comment '角色|id',
    `create_user` varchar(50) default 'system' comment '创建人',
    `create_time` timestamp default CURRENT_TIMESTAMP comment '创建时间',
    `update_user` varchar(50) default 'system' comment '修改人',
    `update_time` timestamp  default CURRENT_TIMESTAMP comment '修改时间',
    PRIMARY KEY (`id`)
) ENGINE = InnoDB CHARACTER SET = utf8mb4 comment '用户角色';

insert into `user_role`(id,user_id,role_id) values('00000000','10000000','00000000');

# 日志
drop table if exists `sys_log`;
create table `sys_log` (
    `id` varchar(32)  NOT NULL comment 'id',
    `business_id` varchar(32) comment '业务流水号',
    `description` varchar(100) comment '操作描述|枚举[OperationTypeEnum]',
    `ip` varchar(32) comment '请求ip',
    `service` varchar(32) comment '服务名',
    `method` varchar(100) comment '方法名',
    `result` char(1) comment '处理结果|枚举[OperationResultEnum]：YES("1", "成功"),NO("2", "失败");',
    `user` varchar(32) comment '操作人',
    `consume_time` bigint comment '消耗时间',
    `create_user` varchar(50) default 'system' comment '创建人',
    `create_time` timestamp default CURRENT_TIMESTAMP comment '创建时间',
    `update_user` varchar(50) default 'system' comment '修改人',
    `update_time` timestamp  default CURRENT_TIMESTAMP comment '修改时间',
    PRIMARY KEY (`id`)
) ENGINE = InnoDB CHARACTER SET = utf8mb4 comment '日志';

# 日志详情
drop table if exists `sys_log_detail`;
create table `sys_log_detail` (
    `id` varchar(32)  NOT NULL comment '日志id',
    `request` varchar(2500) comment '请求参数',
    `response` varchar(2500) comment '响应参数',
    `create_user` varchar(50) default 'system' comment '创建人',
    `create_time` timestamp default CURRENT_TIMESTAMP comment '创建时间',
    `update_user` varchar(50) default 'system' comment '修改人',
    `update_time` timestamp  default CURRENT_TIMESTAMP comment '修改时间',
    PRIMARY KEY (`id`)
) ENGINE = InnoDB CHARACTER SET = utf8mb4 comment '日志详情';