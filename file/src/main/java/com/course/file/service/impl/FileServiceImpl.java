package com.course.file.service.impl;

import com.course.file.service.FileService;
import com.course.server.domain.File;
import com.course.server.domain.FileExample;
import com.course.server.dto.FileDTO;
import com.course.server.dto.PageDTO;
import com.course.server.mapper.FileMapper;
import com.course.server.util.CopyUtil;
import com.course.server.util.UuidUtil;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import javax.annotation.Resource;
import java.util.Date;
import java.util.List;

@Service
public class FileServiceImpl implements FileService {
    @Resource
    private FileMapper fileMapper;

    /**
     * 列表查询
     *
     * @param pageDTO
     */
    @Override
    public void list(PageDTO<FileDTO> pageDTO) {
        PageHelper.startPage(pageDTO.getPage(), pageDTO.getSize());
        FileExample fileExample = new FileExample();


        List<File> fileList = fileMapper.selectByExample(fileExample);
        PageInfo<File> pageInfo = new PageInfo<>(fileList);
        pageDTO.setTotal(pageInfo.getTotal());
        List<FileDTO> fileDTOList = CopyUtil.copyList(fileList, FileDTO.class);
        pageDTO.setList(fileDTOList);
    }

    /**
     * 保存，id有值时更新，无值时新增
     *
     * @param fileDTO
     * @return
     */
    @Override
    public boolean save(FileDTO fileDTO) {
        File file = CopyUtil.copy(fileDTO, File.class);
        File fileDb = selectByKey(file.getKey());

        boolean result;
        if (fileDb == null) {
            result = insert(file);
        } else {
            fileDb.setShardIndex(fileDTO.getShardIndex());
            result = update(fileDb);
        }
        return result;
    }

    /**
     * 删除
     *
     * @param id
     * @return
     */
    @Override
    public boolean delete(String id) {
        return fileMapper.deleteByPrimaryKey(id) == 1;
    }

    /**
     * 根据key查寻fileDTO
     * @param key
     * @return
     */
    @Override
    public FileDTO findBykey(String key) {
        File file = selectByKey(key);
        return CopyUtil.copy(file, FileDTO.class);
    }


    /**
     * 根据key查寻file
     *
     * @param key
     * @return
     */
    private File selectByKey(String key) {
        FileExample fileExample = new FileExample();
        FileExample.Criteria criteria = fileExample.createCriteria();
        criteria.andKeyEqualTo(key);
        List<File> fileList = fileMapper.selectByExample(fileExample);
        if (CollectionUtils.isEmpty(fileList)) {
            return null;
        }
        return fileList.get(0);
    }

    private boolean insert(File file) {
        file.setId(UuidUtil.getShortUuid());
        return fileMapper.insert(file) == 1;
    }

    private boolean update(File file) {
        file.setUpdateTime(new Date());
        return fileMapper.updateByPrimaryKey(file) == 1;
    }

}
