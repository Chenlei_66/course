package com.course.file.service;

import com.course.server.dto.FileDTO;
import com.course.server.dto.PageDTO;

public interface FileService {
    /**
     * 列表查询
     * @param pageDTO
     */
    void list(PageDTO<FileDTO> pageDTO);

    /**
     * 保存，id有值时更新，无值时新增
     * @param fileDTO
     * @return
     */
    boolean save(FileDTO fileDTO);

    /**
     * 删除
     * @param id
     * @return
     */
    boolean delete(String id);

    /**
     * 根据key查寻fileDTO
     *
     * @param key
     * @return
     */
    FileDTO findBykey(String key);
}
