package com.course.file.controller;

import com.course.server.dto.ResponseDTO;
import com.course.server.exception.ValidatorException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * 全局异常捕获处理类
 */
@ControllerAdvice
public class ControllerExceptionHandler {
    private static final Logger LOG = LoggerFactory.getLogger(ControllerExceptionHandler.class);

    @ResponseBody
    @ExceptionHandler(value = ValidatorException.class)
    public ResponseDTO validatorExceptionHandler(ValidatorException e){
        ResponseDTO responseDTO = new ResponseDTO();
        responseDTO.setSuccess(false);
        LOG.warn(e.getMessage());
        responseDTO.setMessage("请求参数异常");
        return responseDTO;
    }


    @ResponseBody
    @ExceptionHandler(value = Exception.class)
    public ResponseDTO otherExceptionHandler(Exception e){
        ResponseDTO responseDTO = new ResponseDTO();
        responseDTO.setSuccess(false);
        LOG.error("未知异常：",e);
        responseDTO.setMessage("未知异常");
        return responseDTO;
    }
}
