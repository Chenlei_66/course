package com.course.file.controller.admin;

import com.course.file.service.FileService;
import com.course.server.dto.FileDTO;
import com.course.server.dto.PageDTO;
import com.course.server.dto.ResponseDTO;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

@RestController
@RequestMapping("admin/file")
public class FileController {

    public static final String BUSINESS_NAME = "文件";

    @Resource
    private FileService fileService;

    /**
     * 列表查询
     *
     * @param pageDTO
     * @return
     */
    @RequestMapping(value = "/list", method = RequestMethod.POST, produces = {"application/json;charset=UTF-8"})
    public ResponseDTO list(@RequestBody PageDTO<FileDTO> pageDTO) {
        ResponseDTO<PageDTO<FileDTO>> responseDTO = new ResponseDTO<>();
        fileService.list(pageDTO);
        responseDTO.setContent(pageDTO);
        return responseDTO;
    }
}
