package com.course.file.controller.admin;

import com.course.file.service.FileService;
import com.course.server.dto.FileDTO;
import com.course.server.dto.ResponseDTO;
import com.course.server.enums.FileUseEnum;
import com.course.server.util.Base64ToMultipartFileUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;

/**
 * 本地服务器文件上传Controller层
 */
@RestController
@RequestMapping("admin")
public class UploadController {

    public static final String BUSINESS_NAME = "文件上传";

    private static final Logger LOG = LoggerFactory.getLogger(UploadController.class);

    @Value("${file.domain}")
    private String FILE_DOMAIN;
    @Value("${file.path}")
    private String FILE_PATH;

    @Resource
    private FileService fileService;

    /**
     * 文件上传
     *
     * @return
     */
    @RequestMapping(value = "/upload", method = RequestMethod.POST, produces = {"application/json;charset=UTF-8"})
    public ResponseDTO<Object> upload(@RequestBody FileDTO fileDTO) throws Exception {
        LOG.info("上传文件开始");
        ResponseDTO<Object> responseDTO = new ResponseDTO<>();

        String use = fileDTO.getUse();
        String key = fileDTO.getKey();
        String suffix = fileDTO.getSuffix();
        String shardBase64 = fileDTO.getShard();
        MultipartFile shard = Base64ToMultipartFileUtil.base64ToMultipart(shardBase64);
        if(shard == null) {
            responseDTO.setSuccess(false);
            responseDTO.setMessage("文件获取异常");
            return responseDTO;
        }

        // 保存文件到本地
        FileUseEnum useEnum = FileUseEnum.getByCode(use);
        // 如果文件夹不存在则创建
        String dir = useEnum.name().toLowerCase();
        File fullDir = new File(FILE_PATH + dir);
        if (!fullDir.exists()) {
            fullDir.mkdirs();
        }

        String path = dir + File.separator + key + "." + suffix;

        String localPath = path + "." + fileDTO.getShardIndex();

        String fullPath = FILE_PATH + localPath;
        File dest = new File(fullPath);
        shard.transferTo(dest);
        LOG.info(dest.getAbsolutePath());

        // 保存文件记录
        LOG.info("保存文件记录开始");
        fileDTO.setPath(path);
        fileService.save(fileDTO);


        fileDTO.setPath(FILE_DOMAIN + path);
        responseDTO.setContent(fileDTO);

        // 合并分片
        if (fileDTO.getShardIndex().equals(fileDTO.getShardTotal())) {
            merge(fileDTO);
        }

        return responseDTO;
    }

    /**
     * 文件分片检查
     *
     * @return
     */
    @RequestMapping(value = "/check/{key}", method = RequestMethod.POST, produces = {"application/json;charset=UTF-8"})
    public ResponseDTO<FileDTO> check(@PathVariable String key) {
        ResponseDTO<FileDTO> responseDTO = new ResponseDTO<>();
        FileDTO fileDTO = fileService.findBykey(key);
        if(fileDTO != null) {
            fileDTO.setPath(FILE_DOMAIN + fileDTO.getPath());
        }
        responseDTO.setContent(fileDTO);
        return responseDTO;
    }

    /**
     * 分片文件合并
     *
     * @return
     * @throws Exception
     */
    public void merge(FileDTO fileDTO) throws Exception {
        LOG.info("合并分片开始");
        String path = fileDTO.getPath();
        path = path.replace(FILE_DOMAIN, "");
        Integer shardTotal = fileDTO.getShardTotal();
        File newFile = new File(FILE_PATH + path);
        FileOutputStream outputStream = new FileOutputStream(newFile, true);
        FileInputStream fileInputStream = null;

        byte[] byt = new byte[10 * 1024 * 1024];
        int len;

        try {
            for (int i = 0; i < shardTotal; i++) {
                try {
                    fileInputStream = new FileInputStream(new File(FILE_PATH + path + "." + (i + 1)));
                    while ((len = fileInputStream.read(byt)) != -1) {
                        outputStream.write(byt, 0, len);
                    }
                } finally {
                    if (fileInputStream != null) {
                        fileInputStream.close();
                    }
                }
            }

        } catch (Exception e) {
            LOG.error("分片合并异常", e);
        } finally {
            try {
                outputStream.close();
                LOG.info("IO流关闭");
            } catch (Exception e) {
                LOG.error("IO流程关闭", e);
            }
        }
        LOG.info("合并分片结束");

        // 垃圾回收，确保jvm不再占用文件资源，保证文件删除成功
        System.gc();
        Thread.sleep(100);

        // 删除分片
        LOG.info("删除分片开始");
        for (int i = 0; i < shardTotal; i++) {
            String filePath = FILE_PATH + path + "." + (i + 1);
            File file = new File(filePath);
            boolean result = file.delete();
            LOG.info("删除{}，{}", filePath, result ? "成功" : "失败");
        }
        LOG.info("删除分片结束");
    }

}
