package com.course.file.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

/**
 * @author cl
 * @description SpringBoot静态资源配置(图片 、 css 、 js等)
 * @date 2020/8/17 23:22
 */
@Configuration
public class SpringMvcConfig implements WebMvcConfigurer {

    @Value("${file.path}")
    private String FILE_PATH;

    /**
     * springboot通过设置addResourceHandlers拦截请求访问本地资源配置
     * 例：通过http://127.0.0.1:9003/file/f/teacher/vHQhfzGm-头像2.jpg
     * 访问D:/file/lei/course/teacher/vHQhfzGm-头像2.jpg
     *
     * @param registry
     */
    @Override
    public void addResourceHandlers(ResourceHandlerRegistry registry) {
        registry.addResourceHandler("/f/**")
                .addResourceLocations("file:" + FILE_PATH);
    }


}
