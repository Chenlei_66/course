## 模块名称：业务模块

## 功能描述：
- 课程管理
- 大章管理
- 小节管理
- 讲师管理


## 4.开发环境：
    【罗列使用本工程项目所需要安装的开发环境及配置】

## 历史版本
 | 序号 | 版本号 | 发版日期 | 更新内容 |
 | :------: | :------: | :------: | :------ |
 | 1 | V1.0.0 | 20201011 | 初始版本 |

## 测试 || DEMO:

