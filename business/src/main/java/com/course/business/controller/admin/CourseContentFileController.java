package com.course.business.controller.admin;

import com.course.business.service.CourseContentFileService;
import com.course.server.dto.CourseContentFileDTO;
import com.course.server.dto.ResponseDTO;
import com.course.server.util.ValidatorUtil;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;

@RestController
@RequestMapping("admin/courseContentFile")
public class CourseContentFileController {

    public static final String BUSINESS_NAME = "课程内容文件";

    @Resource
    private CourseContentFileService courseContentFileService;

    /**
     * 列表查询
     * @param courseId
     * @return
     */
    @RequestMapping(value = "/list/{courseId}", method = RequestMethod.POST, produces = {"application/json;charset=UTF-8"})
    public ResponseDTO list(@PathVariable String courseId) {
        ResponseDTO<List<CourseContentFileDTO>> responseDTO = new ResponseDTO<>();
        List<CourseContentFileDTO> list = courseContentFileService.list(courseId);
        responseDTO.setContent(list);
        return responseDTO;
    }

    /**
     * 保存，id有值时更新，无值时新增
     * @param courseContentFileDTO
     * @return
     */
    @RequestMapping(value = "/save", method = RequestMethod.POST, produces = {"application/json;charset=UTF-8"})
    public ResponseDTO save(@RequestBody CourseContentFileDTO courseContentFileDTO) {
        // 保存校验
        ValidatorUtil.require(courseContentFileDTO.getCourseId(),"课程id");
        ValidatorUtil.length(courseContentFileDTO.getCourseId(),"课程id",1,32);

        ResponseDTO<CourseContentFileDTO> responseDTO = new ResponseDTO<>();
        boolean success = courseContentFileService.save(courseContentFileDTO);
        responseDTO.setSuccess(success);
        if(success){
            responseDTO.setContent(courseContentFileDTO);
        }
        return responseDTO;
    }

    /**
     * 删除
     * @param id
     * @return
     */
    @DeleteMapping(value = "/delete/{id}", produces = {"application/json;charset=UTF-8"})
    public ResponseDTO delete(@PathVariable String id) {
        ResponseDTO<CourseContentFileDTO> responseDTO = new ResponseDTO<>();
        boolean success = courseContentFileService.delete(id);
        responseDTO.setSuccess(success);
        return responseDTO;
    }
}
