package com.course.business.controller.admin;

import com.course.business.service.CategoryService;
import com.course.server.dto.CategoryDTO;
import com.course.server.dto.PageDTO;
import com.course.server.dto.ResponseDTO;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;

@RestController
@RequestMapping("admin/category")
public class CategoryController {

    public static final String BUSINESS_NAME = "分类";

    @Resource
    private CategoryService categoryService;

    /**
     * 全量查询
     * @return
     */
    @RequestMapping(value = "/all", method = RequestMethod.POST, produces = {"application/json;charset=UTF-8"})
    public ResponseDTO all() {
        ResponseDTO<List<CategoryDTO>> responseDTO = new ResponseDTO<>();
        List<CategoryDTO> categoryDTOList = categoryService.all();
        responseDTO.setContent(categoryDTOList);
        return responseDTO;
    }

    /**
     * 列表查询
     * @param pageDTO
     * @return
     */
    @RequestMapping(value = "/list", method = RequestMethod.POST, produces = {"application/json;charset=UTF-8"})
    public ResponseDTO list(@RequestBody PageDTO<CategoryDTO> pageDTO) {
        ResponseDTO<PageDTO<CategoryDTO>> responseDTO = new ResponseDTO<>();
        categoryService.list(pageDTO);
        responseDTO.setContent(pageDTO);
        return responseDTO;
    }

    /**
     * 保存，id有值时更新，无值时新增
     * @param categoryDTO
     * @return
     */
    @RequestMapping(value = "/save", method = RequestMethod.POST, produces = {"application/json;charset=UTF-8"})
    public ResponseDTO save(@RequestBody CategoryDTO categoryDTO) {
        // 保存校验

        ResponseDTO<CategoryDTO> responseDTO = new ResponseDTO<>();
        boolean success = categoryService.save(categoryDTO);
        responseDTO.setSuccess(success);
        if(success){
            responseDTO.setContent(categoryDTO);
        }
        return responseDTO;
    }

    /**
     * 删除
     * @param id
     * @return
     */
    @DeleteMapping(value = "/delete/{id}", produces = {"application/json;charset=UTF-8"})
    public ResponseDTO delete(@PathVariable String id) {
        ResponseDTO<CategoryDTO> responseDTO = new ResponseDTO<>();
        boolean success = categoryService.delete(id);
        responseDTO.setSuccess(success);
        return responseDTO;
    }
}
