package com.course.business.controller.admin;

import com.course.business.service.SectionService;
import com.course.server.annotation.LogToDb;
import com.course.server.dto.ResponseDTO;
import com.course.server.dto.SectionDTO;
import com.course.server.dto.SectionPageDTO;
import com.course.server.enums.OperationTypeEnum;
import com.course.server.util.ValidatorUtil;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.Map;

@RestController
@RequestMapping("admin/section")
public class SectionController {

    public static final String BUSINESS_NAME = "小节";

    @Resource
    private SectionService sectionService;

    /**
     * 列表查询
     *
     * @param sectionPageDTO
     * @return
     */
    @RequestMapping(value = "/list", method = RequestMethod.POST, produces = {"application/json;charset=UTF-8"})
    public ResponseDTO<SectionPageDTO> list(@RequestBody SectionPageDTO sectionPageDTO) {
        ResponseDTO<SectionPageDTO> responseDTO = new ResponseDTO<>();
        ValidatorUtil.require(sectionPageDTO.getCourseId(), "课程ID");
        ValidatorUtil.require(sectionPageDTO.getChapterId(), "大章ID");
        sectionService.list(sectionPageDTO);
        responseDTO.setContent(sectionPageDTO);
        return responseDTO;
    }

    /**
     * 保存，id有值时更新，无值时新增
     *
     * @param sectionDTO
     * @return
     */
    @LogToDb(OperationTypeEnum.SECTION_SAVE)
    @RequestMapping(value = "/save", method = RequestMethod.POST, produces = {"application/json;charset=UTF-8"})
    public ResponseDTO save(@RequestBody SectionDTO sectionDTO) {
        // 保存校验
        ValidatorUtil.require(sectionDTO.getTitle(), "标题");
        ValidatorUtil.length(sectionDTO.getTitle(), "标题", 1, 50);

        ResponseDTO<SectionDTO> responseDTO = new ResponseDTO<>();
        boolean success = sectionService.save(sectionDTO);
        responseDTO.setSuccess(success);
        if (success) {
            responseDTO.setContent(sectionDTO);
        }
        return responseDTO;
    }

    /**
     * 删除
     *
     * @param id
     * @return
     */
    @LogToDb(OperationTypeEnum.SECTION_DEL)
    @DeleteMapping(value = "/delete/{id}", produces = {"application/json;charset=UTF-8"})
    public ResponseDTO delete(@PathVariable String id) {
        ResponseDTO<SectionDTO> responseDTO = new ResponseDTO<>();
        boolean success = sectionService.delete(id);
        responseDTO.setSuccess(success);
        return responseDTO;
    }

    @LogToDb(OperationTypeEnum.SECTION_DEL)
    @PostMapping(value = "/test", produces = {"application/json;charset=UTF-8"})
    public String helloWorld(@RequestBody Map<String,Object> request){
        return "hello-world";
    }
}
