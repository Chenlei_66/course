package com.course.business.controller.admin;

import com.course.business.service.ChapterService;
import com.course.server.dto.ChapterDTO;
import com.course.server.dto.ChapterPageDTO;
import com.course.server.dto.PageDTO;
import com.course.server.dto.ResponseDTO;
import com.course.server.util.ValidatorUtil;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;

@RestController
@RequestMapping("admin/chapter")
public class ChapterController {

    public static final String BUSINESS_NAME = "大章";

    @Resource
    private ChapterService chapterService;

    /**
     * 列表查询
     * @param chapterPageDTO
     * @return
     */
    @RequestMapping(value = "/list", method = RequestMethod.POST, produces = {"application/json;charset=UTF-8"})
    public ResponseDTO list(@RequestBody ChapterPageDTO chapterPageDTO) {
        ResponseDTO<PageDTO<ChapterDTO>> responseDTO = new ResponseDTO<>();
        ValidatorUtil.require(chapterPageDTO.getCourseId(), "课程ID");
        chapterService.list(chapterPageDTO);
        responseDTO.setContent(chapterPageDTO);
        return responseDTO;
    }

    /**
     * 保存，id有值时更新，无值时新增
     * @param chapterDTO
     * @return
     */
    @RequestMapping(value = "/save", method = RequestMethod.POST, produces = {"application/json;charset=UTF-8"})
    public ResponseDTO save(@RequestBody ChapterDTO chapterDTO) {
        // 保存校验
        ValidatorUtil.require(chapterDTO.getName(), "名称");
        ValidatorUtil.require(chapterDTO.getCourseId(), "课程ID");
        ValidatorUtil.length(chapterDTO.getCourseId(), "课程ID",1,32);

        ResponseDTO<ChapterDTO> responseDTO = new ResponseDTO<>();
        boolean success = chapterService.save(chapterDTO);
        responseDTO.setSuccess(success);
        if(success){
            responseDTO.setContent(chapterDTO);
        }
        return responseDTO;
    }

    /**
     * 删除
     * @param id
     * @return
     */
    @DeleteMapping(value = "/delete/{id}", produces = {"application/json;charset=UTF-8"})
    public ResponseDTO delete(@PathVariable String id) {
        ResponseDTO<ChapterDTO> responseDTO = new ResponseDTO<>();
        boolean success = chapterService.delete(id);
        responseDTO.setSuccess(success);
        return responseDTO;
    }
}
