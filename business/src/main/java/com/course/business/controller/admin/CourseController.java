package com.course.business.controller.admin;

import com.course.business.service.CourseCategoryService;
import com.course.business.service.CourseService;
import com.course.server.dto.*;
import com.course.server.util.ValidatorUtil;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;

@RestController
@RequestMapping("admin/course")
public class CourseController {

    public static final String BUSINESS_NAME = "课程";

    @Resource
    private CourseService courseService;
    @Resource
    private CourseCategoryService courseCategoryService;

    /**
     * 列表查询
     * @param pageDTO
     * @return
     */
    @RequestMapping(value = "/list", method = RequestMethod.POST, produces = {"application/json;charset=UTF-8"})
    public ResponseDTO<PageDTO<CourseDTO>> list(@RequestBody PageDTO<CourseDTO> pageDTO) {
        ResponseDTO<PageDTO<CourseDTO>> responseDTO = new ResponseDTO<>();
        courseService.list(pageDTO);
        responseDTO.setContent(pageDTO);
        return responseDTO;
    }

    /**
     * 保存，id有值时更新，无值时新增
     * @param courseDTO
     * @return
     */
    @RequestMapping(value = "/save", method = RequestMethod.POST, produces = {"application/json;charset=UTF-8"})
    public ResponseDTO<CourseDTO> save(@RequestBody CourseDTO courseDTO) {
        // 保存校验
        ValidatorUtil.require(courseDTO.getName(),"名称");
        ValidatorUtil.length(courseDTO.getName(),"名称",1,50);

        ResponseDTO<CourseDTO> responseDTO = new ResponseDTO<>();
        boolean success = courseService.save(courseDTO);
        responseDTO.setSuccess(success);
        if(success){
            responseDTO.setContent(courseDTO);
        }
        return responseDTO;
    }

    /**
     * 删除
     * @param id
     * @return
     */
    @DeleteMapping(value = "/delete/{id}", produces = {"application/json;charset=UTF-8"})
    public ResponseDTO<CourseDTO> delete(@PathVariable String id) {
        ResponseDTO<CourseDTO> responseDTO = new ResponseDTO<>();
        boolean success = courseService.delete(id);
        responseDTO.setSuccess(success);
        return responseDTO;
    }

    /**
     * 查找课程下所有的分类
     * @param courseId
     * @return
     */
    @RequestMapping(value = "/listCategory/{courseId}", method = RequestMethod.POST, produces = {"application/json;charset=UTF-8"})
    public ResponseDTO<List<CourseCategoryDTO>> listCategory(@PathVariable String courseId){
        ResponseDTO<List<CourseCategoryDTO>> responseDTO = new ResponseDTO<>();
        List<CourseCategoryDTO> dtoList = courseCategoryService.listByCourse(courseId);
        responseDTO.setContent(dtoList);
        return responseDTO;
    }

    /**
     * 查找课程内容
     * @param id
     * @return
     */
    @RequestMapping(value = "/findContent/{id}", method = RequestMethod.POST, produces = {"application/json;charset=UTF-8"})
    public ResponseDTO<CourseContentDTO> findContent(@PathVariable String id){
        ResponseDTO<CourseContentDTO> responseDTO = new ResponseDTO<>();
        CourseContentDTO contentDTO = courseService.findContent(id);
        responseDTO.setContent(contentDTO);
        return responseDTO;
    }

    /**
     * 保存课程内容
     * @param courseContentDTO
     * @return
     */
    @RequestMapping(value = "/saveContent", method = RequestMethod.POST, produces = {"application/json;charset=UTF-8"})
    public ResponseDTO<CourseContentDTO> saveContent(@RequestBody CourseContentDTO courseContentDTO){
        ResponseDTO<CourseContentDTO> responseDTO = new ResponseDTO<>();
        boolean success = courseService.saveContent(courseContentDTO);
        responseDTO.setSuccess(success);
        return responseDTO;
    }


    /**
     * 更新课程排序
     * @param sortDTO
     * @return
     */
    @RequestMapping(value = "/sort", method = RequestMethod.POST, produces = {"application/json;charset=UTF-8"})
    public ResponseDTO sort(@RequestBody SortDTO sortDTO){
        ResponseDTO responseDTO = new ResponseDTO<>();
        boolean success = courseService.sort(sortDTO);
        responseDTO.setSuccess(success);
        return responseDTO;
    }
}
