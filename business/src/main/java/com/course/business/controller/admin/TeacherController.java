package com.course.business.controller.admin;

import com.course.business.service.TeacherService;
import com.course.server.dto.PageDTO;
import com.course.server.dto.ResponseDTO;
import com.course.server.dto.TeacherDTO;
import com.course.server.util.ValidatorUtil;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;

@RestController
@RequestMapping("admin/teacher")
public class TeacherController {

    public static final String BUSINESS_NAME = "讲师";

    @Resource
    private TeacherService teacherService;

    /**
     * 全量查询
     *
     * @return
     */
    @RequestMapping(value = "/all", method = RequestMethod.POST, produces = {"application/json;charset=UTF-8"})
    public ResponseDTO all() {
        ResponseDTO<List<TeacherDTO>> responseDTO = new ResponseDTO<>();
        List<TeacherDTO> teacherDTOList = teacherService.all();
        responseDTO.setContent(teacherDTOList);
        return responseDTO;
    }

    /**
     * 列表查询
     *
     * @param pageDTO
     * @return
     */
    @RequestMapping(value = "/list", method = RequestMethod.POST, produces = {"application/json;charset=UTF-8"})
    public ResponseDTO list(@RequestBody PageDTO<TeacherDTO> pageDTO) {
        ResponseDTO<PageDTO<TeacherDTO>> responseDTO = new ResponseDTO<>();
        teacherService.list(pageDTO);
        responseDTO.setContent(pageDTO);
        return responseDTO;
    }

    /**
     * 保存，id有值时更新，无值时新增
     *
     * @param teacherDTO
     * @return
     */
    @RequestMapping(value = "/save", method = RequestMethod.POST, produces = {"application/json;charset=UTF-8"})
    public ResponseDTO save(@RequestBody TeacherDTO teacherDTO) {
        // 保存校验
        ValidatorUtil.require(teacherDTO.getName(), "姓名");
        ValidatorUtil.length(teacherDTO.getName(), "姓名", 1, 50);

        ResponseDTO<TeacherDTO> responseDTO = new ResponseDTO<>();
        boolean success = teacherService.save(teacherDTO);
        responseDTO.setSuccess(success);
        if (success) {
            responseDTO.setContent(teacherDTO);
        }
        return responseDTO;
    }

    /**
     * 删除
     *
     * @param id
     * @return
     */
    @DeleteMapping(value = "/delete/{id}", produces = {"application/json;charset=UTF-8"})
    public ResponseDTO delete(@PathVariable String id) {
        ResponseDTO<TeacherDTO> responseDTO = new ResponseDTO<>();
        boolean success = teacherService.delete(id);
        responseDTO.setSuccess(success);
        return responseDTO;
    }
}
