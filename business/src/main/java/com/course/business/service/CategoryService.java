package com.course.business.service;

import com.course.server.dto.CategoryDTO;
import com.course.server.dto.PageDTO;

import java.util.List;

public interface CategoryService {

    /**
     * 全量查询
     * @return
     */
    List<CategoryDTO> all();
    /**
     * 列表查询
     * @param pageDTO
     */
    void list(PageDTO<CategoryDTO> pageDTO);

    /**
     * 保存，id有值时更新，无值时新增
     * @param categoryDTO
     * @return
     */
    boolean save(CategoryDTO categoryDTO);

    /**
     * 删除
     * @param id
     * @return
     */
    boolean delete(String id);

    /**
     * 删除子分类
     * @param id
     * @return
     */
    boolean deleteChildren(String id);
}
