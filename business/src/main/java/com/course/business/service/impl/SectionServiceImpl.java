package com.course.business.service.impl;

import com.course.business.service.CourseService;
import com.course.business.service.SectionService;
import com.course.server.domain.Section;
import com.course.server.domain.SectionExample;
import com.course.server.dto.SectionDTO;
import com.course.server.dto.SectionPageDTO;
import com.course.server.enums.SectionChargeEnum;
import com.course.server.mapper.SectionMapper;
import com.course.server.util.CopyUtil;
import com.course.server.util.UuidUtil;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import javax.annotation.Resource;
import java.util.List;

@Service
public class SectionServiceImpl implements SectionService {
    @Resource
    private SectionMapper sectionMapper;
    @Resource
    private CourseService courseService;

    /**
     * 列表查询
     * @param sectionPageDTO
     */
    @Override
    public void list(SectionPageDTO sectionPageDTO){
        PageHelper.startPage(sectionPageDTO.getPage(),sectionPageDTO.getSize());
        SectionExample sectionExample = new SectionExample();
        SectionExample.Criteria criteria = sectionExample.createCriteria();
        if(!StringUtils.isEmpty(sectionPageDTO.getCourseId())){
            criteria.andCourseIdEqualTo(sectionPageDTO.getCourseId());
        }
        if(!StringUtils.isEmpty(sectionPageDTO.getChapterId())){
            criteria.andChapterIdEqualTo(sectionPageDTO.getChapterId());
        }
        sectionExample.setOrderByClause("sort asc");

        List<Section> sectionList = sectionMapper.selectByExample(sectionExample);
        PageInfo<Section> pageInfo = new PageInfo<>(sectionList);
        sectionPageDTO.setTotal(pageInfo.getTotal());
        List<SectionDTO> sectionDTOList = CopyUtil.copyList(sectionList, SectionDTO.class);
        sectionPageDTO.setList(sectionDTOList);
    }

    /**
     * 保存，id有值时更新，无值时新增
     * @param sectionDTO
     * @return
     */
    @Override
    @Transactional
    public boolean save(SectionDTO sectionDTO) {
        Section section = CopyUtil.copy(sectionDTO, Section.class);
        boolean result;
        if(StringUtils.isEmpty(sectionDTO.getId())){
            result = insert(section);
        }else {
            result = update(section);
        }
        courseService.updateTime(sectionDTO.getCourseId());
        return result;
    }

    /**
     * 删除
     * @param id
     * @return
     */
    @Override
    public boolean delete(String id) {
        return sectionMapper.deleteByPrimaryKey(id) == 1;
    }

    private boolean insert(Section section) {
        section.setId(UuidUtil.getShortUuid());
        section.setCharge(SectionChargeEnum.CHARGE.getCode());
        return sectionMapper.insert(section) == 1;
    }

    private boolean update(Section section) {
        return sectionMapper.updateByPrimaryKey(section) == 1;
    }
}
