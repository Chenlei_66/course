package com.course.business.service;

import com.course.server.dto.SectionDTO;
import com.course.server.dto.SectionPageDTO;

public interface SectionService {
    /**
     * 列表查询
     * @param sectionPageDTO
     */
    void list(SectionPageDTO sectionPageDTO);

    /**
     * 保存，id有值时更新，无值时新增
     * @param sectionDTO
     * @return
     */
    boolean save(SectionDTO sectionDTO);

    /**
     * 删除
     * @param id
     * @return
     */
    boolean delete(String id);
}
