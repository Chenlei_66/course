package com.course.business.service;

import com.course.server.dto.ChapterDTO;
import com.course.server.dto.ChapterPageDTO;

public interface ChapterService {
    /**
     * 列表查询
     * @param chapterPageDTO
     */
    void list(ChapterPageDTO chapterPageDTO);

    /**
     * 保存，id有值时更新，无值时新增
     * @param chapterDTO
     * @return
     */
    boolean save(ChapterDTO chapterDTO);

    /**
     * 删除
     * @param id
     * @return
     */
    boolean delete(String id);
}
