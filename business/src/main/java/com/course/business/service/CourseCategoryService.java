package com.course.business.service;

import com.course.server.dto.CategoryDTO;
import com.course.server.dto.CourseCategoryDTO;
import com.course.server.dto.PageDTO;

import java.util.List;

public interface CourseCategoryService {
    /**
     * 列表查询
     * @param pageDTO
     */
    void list(PageDTO<CourseCategoryDTO> pageDTO);

    /**
     * 保存，id有值时更新，无值时新增
     * @param courseCategoryDTO
     * @return
     */
    boolean save(CourseCategoryDTO courseCategoryDTO);

    /**
     * 删除
     * @param id
     * @return
     */
    boolean delete(String id);

    /**
     * 批量保存
     * @param courseId
     * @param dtoList
     * @return
     */
    boolean saveBatch(String courseId, List<CategoryDTO> dtoList);

    /**
     * 查找课程下所有分类
     * @param courseId
     * @return
     */
    List<CourseCategoryDTO> listByCourse(String courseId);
}
