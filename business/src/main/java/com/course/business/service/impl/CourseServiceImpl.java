package com.course.business.service.impl;

import com.course.business.service.CourseCategoryService;
import com.course.business.service.CourseService;
import com.course.server.domain.Course;
import com.course.server.domain.CourseContent;
import com.course.server.domain.CourseExample;
import com.course.server.dto.CourseContentDTO;
import com.course.server.dto.CourseDTO;
import com.course.server.dto.PageDTO;
import com.course.server.dto.SortDTO;
import com.course.server.mapper.CourseContentMapper;
import com.course.server.mapper.CourseMapper;
import com.course.server.mapper.my.MyCourseMapper;
import com.course.server.util.CopyUtil;
import com.course.server.util.UuidUtil;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import javax.annotation.Resource;
import java.util.List;

@Service
public class CourseServiceImpl implements CourseService {
    @Resource
    private CourseMapper courseMapper;
    @Resource
    private MyCourseMapper myCourseMapper;
    @Resource
    CourseCategoryService courseCategoryService;
    @Resource
    CourseContentMapper courseContentMapper;

    /**
     * 列表查询
     * @param pageDTO
     */
    @Override
    public void list(PageDTO<CourseDTO> pageDTO){
        PageHelper.startPage(pageDTO.getPage(),pageDTO.getSize());
        CourseExample courseExample = new CourseExample();
        courseExample.setOrderByClause("sort asc");


        List<Course> courseList = courseMapper.selectByExample(courseExample);
        PageInfo<Course> pageInfo = new PageInfo<>(courseList);
        pageDTO.setTotal(pageInfo.getTotal());
        List<CourseDTO> courseDTOList = CopyUtil.copyList(courseList, CourseDTO.class);
        pageDTO.setList(courseDTOList);
    }

    /**
     * 保存，id有值时更新，无值时新增
     * @param courseDTO
     * @return
     */
    @Override
    @Transactional
    public boolean save(CourseDTO courseDTO) {
        Course course = CopyUtil.copy(courseDTO, Course.class);
        boolean result;
        if(StringUtils.isEmpty(courseDTO.getId())){
            result = insert(course);
        }else {
            result = update(course);
        }

        // 批量保存课程分类
        if(result) {
            result = courseCategoryService.saveBatch(course.getId(), courseDTO.getCategorys());
        }
        return result;
    }

    /**
     * 删除
     * @param id
     * @return
     */
    @Override
    public boolean delete(String id) {
        return courseMapper.deleteByPrimaryKey(id) == 1;
    }

    /**
     * 更新课程时长
     * @param courseId
     * @return
     */
    @Override
    public int updateTime(String courseId) {
        return myCourseMapper.updateTime(courseId);
    }

    /**
     * 查找课程内容
     * @param id 课程id
     * @return
     */
    @Override
    public CourseContentDTO findContent(String id) {
        CourseContent courseContent = courseContentMapper.selectByPrimaryKey(id);
        if(courseContent == null) {
            return null;
        }
        return CopyUtil.copy(courseContent, CourseContentDTO.class);
    }

    /**
     * 保存课程内容，包含新增和修改
     * @param contentDTO 课程内容
     * @return
     */
    @Override
    public boolean saveContent(CourseContentDTO contentDTO) {
        CourseContent courseContent = CopyUtil.copy(contentDTO, CourseContent.class);
        int i = courseContentMapper.updateByPrimaryKeyWithBLOBs(courseContent);
        if(i == 0) {
            i = courseContentMapper.insert(courseContent);
        }
        return i == 1;
    }

    /**
     * 更新课程排序
     * @param sortDTO 排序
     * @return
     */
    @Override
    @Transactional
    public boolean sort(SortDTO sortDTO) {
        boolean result = false;
        // 修改当前记录的排序
        int updateSortResult = myCourseMapper.updateSort(sortDTO);
        if(updateSortResult == 1){
            // 如果排序值变大
            if(sortDTO.getNewSort() > sortDTO.getOldSort()) {
                myCourseMapper.moveSortsForward(sortDTO);
            }
            // 如果排序值变小
            if(sortDTO.getNewSort() < sortDTO.getOldSort()) {
                myCourseMapper.moveSortsBackward(sortDTO);
            }
            result = true;
        }
        return result;
    }


    private boolean insert(Course course) {
        course.setId(UuidUtil.getShortUuid());
        return courseMapper.insert(course) == 1;
    }

    private boolean update(Course course) {
        return courseMapper.updateByPrimaryKey(course) == 1;
    }
}
