package com.course.business.service;

import com.course.server.dto.CourseContentDTO;
import com.course.server.dto.CourseDTO;
import com.course.server.dto.PageDTO;
import com.course.server.dto.SortDTO;

public interface CourseService {
    /**
     * 列表查询
     * @param pageDTO 请求参数
     */
    void list(PageDTO<CourseDTO> pageDTO);

    /**
     * 保存，id有值时更新，无值时新增
     * @param courseDTO 课程信息
     * @return 处理结果
     */
    boolean save(CourseDTO courseDTO);

    /**
     * 删除
     * @param id 课程id
     * @return
     */
    boolean delete(String id);

    /**
     * 更新课程时长
     * @param courseId 课程id
     * @return 更新结果
     */
    int updateTime(String courseId);

    /**
     * 查找课程内容
     * @param id 课程id
     * @return 课程内容
     */
    CourseContentDTO findContent(String id);

    /**
     * 保存课程内容，包含新增和修改
     * @param contentDTO 课程内容
     * @return 保存结果
     */
    boolean saveContent(CourseContentDTO contentDTO);

    /**
     * 更新课程排序
     * @param sortDTO 排序
     * @return 保存结果
     */
    boolean sort(SortDTO sortDTO);
}
