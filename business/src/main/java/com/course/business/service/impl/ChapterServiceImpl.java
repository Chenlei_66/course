package com.course.business.service.impl;

import com.course.business.service.ChapterService;
import com.course.server.domain.Chapter;
import com.course.server.domain.ChapterExample;
import com.course.server.dto.ChapterDTO;
import com.course.server.dto.ChapterPageDTO;
import com.course.server.mapper.ChapterMapper;
import com.course.server.util.CopyUtil;
import com.course.server.util.UuidUtil;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import javax.annotation.Resource;
import java.util.List;

@Service
public class ChapterServiceImpl implements ChapterService {
    @Resource
    private ChapterMapper chapterMapper;

    /**
     * 列表查询
     * @param chapterPageDTO
     */
    @Override
    public void list(ChapterPageDTO chapterPageDTO){
        PageHelper.startPage(chapterPageDTO.getPage(),chapterPageDTO.getSize());
        ChapterExample chapterExample = new ChapterExample();
        ChapterExample.Criteria criteria = chapterExample.createCriteria();
        if(!StringUtils.isEmpty(chapterPageDTO.getCourseId())){
            criteria.andCourseIdEqualTo(chapterPageDTO.getCourseId());
        }
        List<Chapter> chapterList = chapterMapper.selectByExample(chapterExample);
        PageInfo<Chapter> pageInfo = new PageInfo<>(chapterList);
        chapterPageDTO.setTotal(pageInfo.getTotal());
        List<ChapterDTO> chapterDTOList = CopyUtil.copyList(chapterList, ChapterDTO.class);
        chapterPageDTO.setList(chapterDTOList);
    }

    /**
     * 保存，id有值时更新，无值时新增
     * @param chapterDTO
     * @return
     */
    @Override
    public boolean save(ChapterDTO chapterDTO) {
        Chapter chapter = CopyUtil.copy(chapterDTO, Chapter.class);
        boolean result;
        if(StringUtils.isEmpty(chapterDTO.getId())){
            result = insert(chapter);
        }else {
            result = update(chapter);
        }
        return result;
    }

    /**
     * 删除
     * @param id
     * @return
     */
    @Override
    public boolean delete(String id) {
        return chapterMapper.deleteByPrimaryKey(id) == 1;
    }

    private boolean insert(Chapter chapter) {
        chapter.setId(UuidUtil.getShortUuid());
        return chapterMapper.insert(chapter) == 1;
    }

    private boolean update(Chapter chapter) {
        return chapterMapper.updateByPrimaryKey(chapter) == 1;
    }
}
