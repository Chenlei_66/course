package com.course.business.service.impl;

import com.course.business.service.CourseCategoryService;
import com.course.server.domain.CourseCategory;
import com.course.server.domain.CourseCategoryExample;
import com.course.server.dto.CategoryDTO;
import com.course.server.dto.CourseCategoryDTO;
import com.course.server.dto.PageDTO;
import com.course.server.mapper.CourseCategoryMapper;
import com.course.server.util.CopyUtil;
import com.course.server.util.UuidUtil;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import javax.annotation.Resource;
import java.util.List;

@Service
public class CourseCategoryServiceImpl implements CourseCategoryService {
    @Resource
    private CourseCategoryMapper courseCategoryMapper;

    /**
     * 列表查询
     * @param pageDTO
     */
    @Override
    public void list(PageDTO<CourseCategoryDTO> pageDTO){
        PageHelper.startPage(pageDTO.getPage(),pageDTO.getSize());
        CourseCategoryExample courseCategoryExample = new CourseCategoryExample();


        List<CourseCategory> courseCategoryList = courseCategoryMapper.selectByExample(courseCategoryExample);
        PageInfo<CourseCategory> pageInfo = new PageInfo<>(courseCategoryList);
        pageDTO.setTotal(pageInfo.getTotal());
        List<CourseCategoryDTO> courseCategoryDTOList = CopyUtil.copyList(courseCategoryList, CourseCategoryDTO.class);
        pageDTO.setList(courseCategoryDTOList);
    }

    /**
     * 保存，id有值时更新，无值时新增
     * @param courseCategoryDTO
     * @return
     */
    @Override
    public boolean save(CourseCategoryDTO courseCategoryDTO) {
        CourseCategory courseCategory = CopyUtil.copy(courseCategoryDTO, CourseCategory.class);
        boolean result;
        if(StringUtils.isEmpty(courseCategoryDTO.getId())){
            result = insert(courseCategory);
        }else {
            result = update(courseCategory);
        }
        return result;
    }

    /**
     * 删除
     * @param id
     * @return
     */
    @Override
    public boolean delete(String id) {
        return courseCategoryMapper.deleteByPrimaryKey(id) == 1;
    }

    @Transactional
    @Override
    public boolean saveBatch(String courseId, List<CategoryDTO> dtoList) {
        CourseCategoryExample courseCategoryExample = new CourseCategoryExample();
        courseCategoryExample.createCriteria().andCourseIdEqualTo(courseId);
        courseCategoryMapper.deleteByExample(courseCategoryExample);
        for (int i = 0; i < dtoList.size(); i++) {
            CategoryDTO categoryDTO = dtoList.get(i);
            CourseCategory courseCategory = new CourseCategory();
            courseCategory.setId(UuidUtil.getShortUuid());
            courseCategory.setCourseId(courseId);
            courseCategory.setCategoryId(categoryDTO.getId());
            insert(courseCategory);
        }
        return true;
    }

    @Override
    public List<CourseCategoryDTO> listByCourse(String courseId) {
        CourseCategoryExample courseCategoryExample = new CourseCategoryExample();
        CourseCategoryExample.Criteria criteria = courseCategoryExample.createCriteria();
        criteria.andCourseIdEqualTo(courseId);
        List<CourseCategory> courseCategories = courseCategoryMapper.selectByExample(courseCategoryExample);
        return CopyUtil.copyList(courseCategories, CourseCategoryDTO.class);
    }

    private boolean insert(CourseCategory courseCategory) {
        courseCategory.setId(UuidUtil.getShortUuid());
        return courseCategoryMapper.insert(courseCategory) == 1;
    }

    private boolean update(CourseCategory courseCategory) {
        return courseCategoryMapper.updateByPrimaryKey(courseCategory) == 1;
    }
}
