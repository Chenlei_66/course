package com.course.business.service.impl;

import com.course.business.service.CourseContentFileService;
import com.course.server.domain.CourseContentFile;
import com.course.server.domain.CourseContentFileExample;
import com.course.server.dto.CourseContentFileDTO;
import com.course.server.mapper.CourseContentFileMapper;
import com.course.server.util.CopyUtil;
import com.course.server.util.UuidUtil;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import javax.annotation.Resource;
import java.util.List;

@Service
public class CourseContentFileServiceImpl implements CourseContentFileService {
    @Resource
    private CourseContentFileMapper courseContentFileMapper;

    /**
     * 列表查询
     * @param courseId
     */
    @Override
    public List<CourseContentFileDTO> list(String courseId){
        CourseContentFileExample courseContentFileExample = new CourseContentFileExample();
        CourseContentFileExample.Criteria criteria = courseContentFileExample.createCriteria();
        criteria.andCourseIdEqualTo(courseId);
        List<CourseContentFile> courseContentFileList = courseContentFileMapper.selectByExample(courseContentFileExample);
        return CopyUtil.copyList(courseContentFileList, CourseContentFileDTO.class);
    }

    /**
     * 保存，id有值时更新，无值时新增
     * @param courseContentFileDTO
     * @return
     */
    @Override
    public boolean save(CourseContentFileDTO courseContentFileDTO) {
        CourseContentFile courseContentFile = CopyUtil.copy(courseContentFileDTO, CourseContentFile.class);
        boolean result;
        if(StringUtils.isEmpty(courseContentFileDTO.getId())){
            result = insert(courseContentFile);
        }else {
            result = update(courseContentFile);
        }
        return result;
    }

    /**
     * 删除
     * @param id
     * @return
     */
    @Override
    public boolean delete(String id) {
        return courseContentFileMapper.deleteByPrimaryKey(id) == 1;
    }

    private boolean insert(CourseContentFile courseContentFile) {
        courseContentFile.setId(UuidUtil.getShortUuid());
        return courseContentFileMapper.insert(courseContentFile) == 1;
    }

    private boolean update(CourseContentFile courseContentFile) {
        return courseContentFileMapper.updateByPrimaryKey(courseContentFile) == 1;
    }
}
