package com.course.business.service.impl;

import com.course.business.service.CategoryService;
import com.course.server.domain.Category;
import com.course.server.domain.CategoryExample;
import com.course.server.dto.CategoryDTO;
import com.course.server.dto.PageDTO;
import com.course.server.mapper.CategoryMapper;
import com.course.server.util.CopyUtil;
import com.course.server.util.UuidUtil;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import javax.annotation.Resource;
import java.util.List;

@Service
public class CategoryServiceImpl implements CategoryService {
    @Resource
    private CategoryMapper categoryMapper;

    /**
     * 全量查询
     *
     * @return
     */
    @Override
    public List<CategoryDTO> all() {
        CategoryExample categoryExample = new CategoryExample();
        categoryExample.setOrderByClause("sort asc");
        List<Category> categoryList = categoryMapper.selectByExample(categoryExample);
        return CopyUtil.copyList(categoryList, CategoryDTO.class);
    }

    /**
     * 列表查询
     *
     * @param pageDTO
     */
    @Override
    public void list(PageDTO<CategoryDTO> pageDTO) {
        PageHelper.startPage(pageDTO.getPage(), pageDTO.getSize());
        CategoryExample categoryExample = new CategoryExample();
        categoryExample.setOrderByClause("sort asc");


        List<Category> categoryList = categoryMapper.selectByExample(categoryExample);
        PageInfo<Category> pageInfo = new PageInfo<>(categoryList);
        pageDTO.setTotal(pageInfo.getTotal());
        List<CategoryDTO> categoryDTOList = CopyUtil.copyList(categoryList, CategoryDTO.class);
        pageDTO.setList(categoryDTOList);
    }

    /**
     * 保存，id有值时更新，无值时新增
     *
     * @param categoryDTO
     * @return
     */
    @Override
    public boolean save(CategoryDTO categoryDTO) {
        Category category = CopyUtil.copy(categoryDTO, Category.class);
        boolean result;
        if (StringUtils.isEmpty(categoryDTO.getId())) {
            result = insert(category);
        } else {
            result = update(category);
        }
        return result;
    }

    /**
     * 删除
     *
     * @param id
     * @return
     */
    @Override
    @Transactional
    public boolean delete(String id) {
        // 先删子分类
        if (deleteChildren(id)) {
            return categoryMapper.deleteByPrimaryKey(id) == 1;
        }
        return false;
    }

    /**
     * 删除子分类
     *
     * @param id
     * @return
     */
    @Override
    public boolean deleteChildren(String id) {
        Category category = categoryMapper.selectByPrimaryKey(id);
        if ("00000000".equals(category.getParent())) {
            // 如果是一级分类，需要删除其下的子分类
            CategoryExample categoryExample = new CategoryExample();
            CategoryExample.Criteria criteria = categoryExample.createCriteria();
            criteria.andParentEqualTo(category.getId());
            return categoryMapper.deleteByExample(categoryExample) == 1;
        }
        return true;
    }

    private boolean insert(Category category) {
        category.setId(UuidUtil.getShortUuid());
        return categoryMapper.insert(category) == 1;
    }

    private boolean update(Category category) {
        return categoryMapper.updateByPrimaryKey(category) == 1;
    }
}
