package com.course.business.service.impl;

import com.course.business.service.TeacherService;
import com.course.server.domain.Teacher;
import com.course.server.domain.TeacherExample;
import com.course.server.dto.PageDTO;
import com.course.server.dto.TeacherDTO;
import com.course.server.mapper.TeacherMapper;
import com.course.server.util.CopyUtil;
import com.course.server.util.UuidUtil;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import javax.annotation.Resource;
import java.util.List;

@Service
public class TeacherServiceImpl implements TeacherService {
    @Resource
    private TeacherMapper teacherMapper;

    /**
     * 全量查询
     * @return
     */
    @Override
    public List<TeacherDTO> all() {
        TeacherExample teacherExample = new TeacherExample();
        List<Teacher> teacherList = teacherMapper.selectByExample(teacherExample);
        return CopyUtil.copyList(teacherList, TeacherDTO.class);
    }

    /**
     * 列表查询
     * @param pageDTO
     */
    @Override
    public void list(PageDTO<TeacherDTO> pageDTO){
        PageHelper.startPage(pageDTO.getPage(),pageDTO.getSize());
        TeacherExample teacherExample = new TeacherExample();


        List<Teacher> teacherList = teacherMapper.selectByExample(teacherExample);
        PageInfo<Teacher> pageInfo = new PageInfo<>(teacherList);
        pageDTO.setTotal(pageInfo.getTotal());
        List<TeacherDTO> teacherDTOList = CopyUtil.copyList(teacherList, TeacherDTO.class);
        pageDTO.setList(teacherDTOList);
    }

    /**
     * 保存，id有值时更新，无值时新增
     * @param teacherDTO
     * @return
     */
    @Override
    public boolean save(TeacherDTO teacherDTO) {
        Teacher teacher = CopyUtil.copy(teacherDTO, Teacher.class);
        boolean result;
        if(StringUtils.isEmpty(teacherDTO.getId())){
            result = insert(teacher);
        }else {
            result = update(teacher);
        }
        return result;
    }

    /**
     * 删除
     * @param id
     * @return
     */
    @Override
    public boolean delete(String id) {
        return teacherMapper.deleteByPrimaryKey(id) == 1;
    }

    private boolean insert(Teacher teacher) {
        teacher.setId(UuidUtil.getShortUuid());
        return teacherMapper.insert(teacher) == 1;
    }

    private boolean update(Teacher teacher) {
        return teacherMapper.updateByPrimaryKey(teacher) == 1;
    }
}
