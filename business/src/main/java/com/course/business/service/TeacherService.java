package com.course.business.service;

import com.course.server.dto.PageDTO;
import com.course.server.dto.TeacherDTO;

import java.util.List;

public interface TeacherService {

    /**
     * 全量查询
     * @return
     */
    List<TeacherDTO> all();

    /**
     * 列表查询
     * @param pageDTO
     */
    void list(PageDTO<TeacherDTO> pageDTO);

    /**
     * 保存，id有值时更新，无值时新增
     * @param teacherDTO
     * @return
     */
    boolean save(TeacherDTO teacherDTO);

    /**
     * 删除
     * @param id
     * @return
     */
    boolean delete(String id);
}
