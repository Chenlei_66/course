package com.course.business.service;

import com.course.server.dto.CourseContentFileDTO;

import java.util.List;

public interface CourseContentFileService {
    /**
     * 列表查询
     * @param courseId
     */
    List<CourseContentFileDTO> list(String courseId);

    /**
     * 保存，id有值时更新，无值时新增
     * @param courseContentFileDTO
     * @return
     */
    boolean save(CourseContentFileDTO courseContentFileDTO);

    /**
     * 删除
     * @param id
     * @return
     */
    boolean delete(String id);
}
