Loading = {
  show: function () {
    $.blockUI({
      message: '<img src="/static/image/loading.gif" />',
      css: {
        zIndex: "10011", // 避免loading框被模态框阻挡，看不到等待效果
        padding: "10px",
        left: "50%",
        width: "80px",
        marginLeft: "-40px",
      }
    })
  },

  hide: function () {
    $.unblockUI();
/*    // 本地查询速度快，loading显示一瞬间，故意做个延迟
    setTimeout(function () {
      $.unblockUI();
    }, 500)*/
  }
};