package com.course.system.service.impl;

import com.course.server.domain.RoleResource;
import com.course.server.domain.RoleResourceExample;
import com.course.server.dto.PageDTO;
import com.course.server.dto.RoleResourceDTO;
import com.course.server.mapper.RoleResourceMapper;
import com.course.server.util.CopyUtil;
import com.course.server.util.UuidUtil;
import com.course.system.service.RoleResourceService;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import javax.annotation.Resource;
import java.util.List;

@Service
public class RoleResourceServiceImpl implements RoleResourceService {
    @Resource
    private RoleResourceMapper roleResourceMapper;

    /**
     * 列表查询
     * @param pageDTO
     */
    @Override
    public void list(PageDTO<RoleResourceDTO> pageDTO){
        PageHelper.startPage(pageDTO.getPage(),pageDTO.getSize());
        RoleResourceExample roleResourceExample = new RoleResourceExample();


        List<RoleResource> roleResourceList = roleResourceMapper.selectByExample(roleResourceExample);
        PageInfo<RoleResource> pageInfo = new PageInfo<>(roleResourceList);
        pageDTO.setTotal(pageInfo.getTotal());
        List<RoleResourceDTO> roleResourceDTOList = CopyUtil.copyList(roleResourceList, RoleResourceDTO.class);
        pageDTO.setList(roleResourceDTOList);
    }

    /**
     * 保存，id有值时更新，无值时新增
     * @param roleResourceDTO
     * @return
     */
    @Override
    public boolean save(RoleResourceDTO roleResourceDTO) {
        RoleResource roleResource = CopyUtil.copy(roleResourceDTO, RoleResource.class);
        boolean result;
        if(StringUtils.isEmpty(roleResourceDTO.getId())){
            result = insert(roleResource);
        }else {
            result = update(roleResource);
        }
        return result;
    }

    /**
     * 删除
     * @param id
     * @return
     */
    @Override
    public boolean delete(String id) {
        return roleResourceMapper.deleteByPrimaryKey(id) == 1;
    }

    private boolean insert(RoleResource roleResource) {
        roleResource.setId(UuidUtil.getShortUuid());
        return roleResourceMapper.insert(roleResource) == 1;
    }

    private boolean update(RoleResource roleResource) {
        return roleResourceMapper.updateByPrimaryKey(roleResource) == 1;
    }
}
