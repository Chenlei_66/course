package com.course.system.service;

import com.course.server.dto.PageDTO;
import com.course.server.dto.RoleDTO;

import java.util.List;

public interface RoleService {
    /**
     * 列表查询
     * @param pageDTO
     */
    void list(PageDTO<RoleDTO> pageDTO);

    /**
     * 保存，id有值时更新，无值时新增
     * @param roleDTO
     * @return
     */
    boolean save(RoleDTO roleDTO);

    /**
     * 删除
     * @param id
     * @return
     */
    boolean delete(String id);

    /**
     * 保存资源
     * @param roleDTO
     */
    void saveResource(RoleDTO roleDTO);

    /**
     * 加载已关联的资源
     * @param roleId
     * @return
     */
    List<String> listResource(String roleId);

    /**
     * 保存用户
     * @param roleDTO
     */
    void saveUser(RoleDTO roleDTO);

    /**
     * 加载已关联的用户
     * @param roleId
     * @return
     */
    List<String> listUser(String roleId);
}
