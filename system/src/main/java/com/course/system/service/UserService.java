package com.course.system.service;

import com.course.server.dto.LoginUserDTO;
import com.course.server.dto.PageDTO;
import com.course.server.dto.UserDTO;

public interface UserService {
    /**
     * 列表查询
     * @param pageDTO
     */
    void list(PageDTO<UserDTO> pageDTO);

    /**
     * 保存，id有值时更新，无值时新增
     * @param userDTO
     * @return
     */
    boolean save(UserDTO userDTO);

    /**
     * 删除
     * @param id
     * @return
     */
    boolean delete(String id);

    /**
     * 根据用户登录名查找用户
     * @param loginName 登录名
     * @return 用户信息
     */
    UserDTO selectByLoginName(String loginName);

    /**
     * 重置密码
     * @param userDTO
     * @return
     */
    boolean savePassword(UserDTO userDTO);

    /**
     * 用户登录
     * @param userDTO
     * @return
     */
    LoginUserDTO login(UserDTO userDTO);
}
