package com.course.system.service;

import com.course.server.dto.PageDTO;
import com.course.server.dto.ResourceDTO;

import java.util.List;

public interface ResourceService {
    /**
     * 列表查询
     * @param pageDTO
     */
    void list(PageDTO<ResourceDTO> pageDTO);

    /**
     * 保存，id有值时更新，无值时新增
     * @param resourceDTO
     * @return
     */
    boolean save(ResourceDTO resourceDTO);

    /**
     * 删除
     * @param id
     * @return
     */
    boolean delete(String id);

    /**
     * 按json字符串批量保存
     * @param jsonStr
     * @return
     */
    boolean saveJson(String jsonStr);

    /**
     * 资源树查询
     * @return
     */
    List<ResourceDTO> loadTree();
}
