package com.course.system.service.impl;

import com.alibaba.fastjson.JSON;
import com.course.server.domain.User;
import com.course.server.domain.UserExample;
import com.course.server.dto.LoginUserDTO;
import com.course.server.dto.PageDTO;
import com.course.server.dto.ResourceDTO;
import com.course.server.dto.UserDTO;
import com.course.server.exception.BusinessException;
import com.course.server.exception.BusinessExceptionCodeEnum;
import com.course.server.mapper.UserMapper;
import com.course.server.mapper.my.MyUserMapper;
import com.course.server.util.CopyUtil;
import com.course.server.util.UuidUtil;
import com.course.system.service.UserService;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;

import javax.annotation.Resource;
import java.util.HashSet;
import java.util.List;

@Service
public class UserServiceImpl implements UserService {
    private static final Logger LOG = LoggerFactory.getLogger(UserServiceImpl.class);
    @Resource
    private UserMapper userMapper;
    @Resource
    private MyUserMapper myUserMapper;

    /**
     * 列表查询
     *
     * @param pageDTO
     */
    @Override
    public void list(PageDTO<UserDTO> pageDTO) {
        PageHelper.startPage(pageDTO.getPage(), pageDTO.getSize());
        UserExample userExample = new UserExample();


        List<User> userList = userMapper.selectByExample(userExample);
        PageInfo<User> pageInfo = new PageInfo<>(userList);
        pageDTO.setTotal(pageInfo.getTotal());
        List<UserDTO> userDTOList = CopyUtil.copyList(userList, UserDTO.class);
        pageDTO.setList(userDTOList);
    }

    /**
     * 保存，id有值时更新，无值时新增
     *
     * @param userDTO
     * @return
     */
    @Override
    public boolean save(UserDTO userDTO) {
        User user = CopyUtil.copy(userDTO, User.class);
        boolean result;
        if (StringUtils.isEmpty(userDTO.getId())) {
            result = insert(user);
        } else {
            result = update(user);
        }
        return result;
    }

    /**
     * 删除
     *
     * @param id
     * @return
     */
    @Override
    public boolean delete(String id) {
        return userMapper.deleteByPrimaryKey(id) == 1;
    }

    /**
     * @param loginName 登录名
     * @return 用户信息
     */
    @Override
    public UserDTO selectByLoginName(String loginName) {
        UserExample userExample = new UserExample();
        userExample.createCriteria().andLoginNameEqualTo(loginName);
        List<User> userList = userMapper.selectByExample(userExample);
        if (CollectionUtils.isEmpty(userList)) {
            return null;
        } else {
            return CopyUtil.copy(userList.get(0), UserDTO.class);
        }
    }

    /**
     * 重置密码
     *
     * @param userDTO
     * @return
     */
    @Override
    public boolean savePassword(UserDTO userDTO) {
        User user = new User();
        user.setId(userDTO.getId());
        user.setPassword(userDTO.getPassword());
        return userMapper.updateByPrimaryKeySelective(user) == 1;
    }

    /**
     * 用户登录
     *
     * @param userDTO 用户登录信息
     * @return
     */
    @Override
    public LoginUserDTO login(UserDTO userDTO) {
        UserDTO userDb = selectByLoginName(userDTO.getLoginName());
        if (userDb == null) {
            LOG.info("用户名不存在，{}", userDTO.getLoginName());
            throw new BusinessException(BusinessExceptionCodeEnum.USER_LOGIN_ERROR);
        }
        if (userDb.getPassword().equals(userDTO.getPassword())) {
            LoginUserDTO loginUserDTO = CopyUtil.copy(userDb, LoginUserDTO.class);
            setAuth(loginUserDTO);
            return loginUserDTO;
        } else {
            LOG.info("密码不正确，输入密码：{},数据库密码：{}", userDTO.getPassword(), userDb.getPassword());
            throw new BusinessException(BusinessExceptionCodeEnum.USER_LOGIN_ERROR);
        }
    }

    private boolean insert(User user) {
        user.setId(UuidUtil.getShortUuid());
        UserDTO userDb = selectByLoginName(user.getLoginName());
        if (userDb != null) {
            throw new BusinessException(BusinessExceptionCodeEnum.USER_LOGIN_NAME_EXIST);
        }
        return userMapper.insert(user) == 1;
    }

    private boolean update(User user) {
        user.setPassword(null);
        return userMapper.updateByPrimaryKeySelective(user) == 1;
    }

    /**
     * 为登录用户读取权限
     *
     * @param loginUserDTO
     */
    private void setAuth(LoginUserDTO loginUserDTO) {
        List<ResourceDTO> resourceDTOList = myUserMapper.findResources(loginUserDTO.getId());
        loginUserDTO.setResources(resourceDTOList);

        // 整理所有权限的要求，用于接口拦截
        HashSet<String> requestSet = new HashSet<>();
        if (!CollectionUtils.isEmpty(resourceDTOList)) {
            for (ResourceDTO resourceDTO : resourceDTOList) {
                String arrayString = resourceDTO.getRequest();
                List<String> requestList = JSON.parseArray(arrayString, String.class);
                if(!CollectionUtils.isEmpty(requestList)) {
                    requestSet.addAll(requestList);
                }
            }
        }
        LOG.info("有权限的请求：{}",requestSet);
        loginUserDTO.setRequests(requestSet);
    }
}
