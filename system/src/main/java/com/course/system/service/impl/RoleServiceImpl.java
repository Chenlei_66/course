package com.course.system.service.impl;

import com.course.server.domain.*;
import com.course.server.dto.PageDTO;
import com.course.server.dto.RoleDTO;
import com.course.server.mapper.RoleMapper;
import com.course.server.mapper.RoleResourceMapper;
import com.course.server.mapper.UserRoleMapper;
import com.course.server.util.CopyUtil;
import com.course.server.util.UuidUtil;
import com.course.system.service.RoleService;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import javax.annotation.Resource;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class RoleServiceImpl implements RoleService {
    @Resource
    private RoleMapper roleMapper;
    @Resource
    private RoleResourceMapper roleResourceMapper;
    @Resource
    private UserRoleMapper userRoleMapper;

    /**
     * 列表查询
     * @param pageDTO
     */
    @Override
    public void list(PageDTO<RoleDTO> pageDTO){
        PageHelper.startPage(pageDTO.getPage(),pageDTO.getSize());
        RoleExample roleExample = new RoleExample();


        List<Role> roleList = roleMapper.selectByExample(roleExample);
        PageInfo<Role> pageInfo = new PageInfo<>(roleList);
        pageDTO.setTotal(pageInfo.getTotal());
        List<RoleDTO> roleDTOList = CopyUtil.copyList(roleList, RoleDTO.class);
        pageDTO.setList(roleDTOList);
    }

    /**
     * 保存，id有值时更新，无值时新增
     * @param roleDTO
     * @return
     */
    @Override
    public boolean save(RoleDTO roleDTO) {
        Role role = CopyUtil.copy(roleDTO, Role.class);
        boolean result;
        if(StringUtils.isEmpty(roleDTO.getId())){
            result = insert(role);
        }else {
            result = update(role);
        }
        return result;
    }

    /**
     * 删除
     * @param id
     * @return
     */
    @Override
    public boolean delete(String id) {
        return roleMapper.deleteByPrimaryKey(id) == 1;
    }

    /**
     * 保存资源
     * @param roleDTO
     */
    @Override
    @Transactional
    public void saveResource(RoleDTO roleDTO) {
        String roleId = roleDTO.getId();
        List<String> resourceIds = roleDTO.getResourceIds();
        // 清空库中当前角色下所有记录
        RoleResourceExample roleResourceExample = new RoleResourceExample();
        roleResourceExample.createCriteria().andRoleIdEqualTo(roleId);
        roleResourceMapper.deleteByExample(roleResourceExample);

        // 保存角色资源
        for (String resourceId : resourceIds) {
            RoleResource roleResource = new RoleResource();
            roleResource.setId(UuidUtil.getShortUuid());
            roleResource.setRoleId(roleId);
            roleResource.setResourceId(resourceId);
            roleResourceMapper.insert(roleResource);
        }
    }

    /**
     * 加载已关联的资源
     * @param roleId
     * @return
     */
    @Override
    public List<String> listResource(String roleId) {
        RoleResourceExample roleResourceExample = new RoleResourceExample();
        roleResourceExample.createCriteria().andRoleIdEqualTo(roleId);
        List<RoleResource> roleResources = roleResourceMapper.selectByExample(roleResourceExample);
        return roleResources.stream()
                .map(RoleResource::getResourceId).collect(Collectors.toList());
    }

    /**
     * 保存资源
     * @param roleDTO
     */
    @Override
    @Transactional
    public void saveUser(RoleDTO roleDTO) {
        String roleId = roleDTO.getId();
        List<String> userIds = roleDTO.getUserIds();
        // 清空库中当前角色下所有记录
        UserRoleExample userRoleExample = new UserRoleExample();
        userRoleExample.createCriteria().andRoleIdEqualTo(roleId);
        userRoleMapper.deleteByExample(userRoleExample);

        // 保存用户角色
        for (String userId : userIds) {
            UserRole userRole = new UserRole();
            userRole.setId(UuidUtil.getShortUuid());
            userRole.setRoleId(roleId);
            userRole.setUserId(userId);
            userRoleMapper.insert(userRole);
        }
    }

    /**
     * 加载已关联的用户
     * @param roleId
     * @return
     */
    @Override
    public List<String> listUser(String roleId) {
        UserRoleExample userRoleExample = new UserRoleExample();
        userRoleExample.createCriteria().andRoleIdEqualTo(roleId);
        List<UserRole> userRoles = userRoleMapper.selectByExample(userRoleExample);
        return userRoles.stream()
                .map(UserRole::getUserId).collect(Collectors.toList());
    }

    private boolean insert(Role role) {
        role.setId(UuidUtil.getShortUuid());
        return roleMapper.insert(role) == 1;
    }

    private boolean update(Role role) {
        return roleMapper.updateByPrimaryKey(role) == 1;
    }
}
