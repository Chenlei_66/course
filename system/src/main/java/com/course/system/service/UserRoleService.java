package com.course.system.service;

import com.course.server.dto.PageDTO;
import com.course.server.dto.UserRoleDTO;

public interface UserRoleService {
    /**
     * 列表查询
     * @param pageDTO
     */
    void list(PageDTO<UserRoleDTO> pageDTO);

    /**
     * 保存，id有值时更新，无值时新增
     * @param userRoleDTO
     * @return
     */
    boolean save(UserRoleDTO userRoleDTO);

    /**
     * 删除
     * @param id
     * @return
     */
    boolean delete(String id);
}
