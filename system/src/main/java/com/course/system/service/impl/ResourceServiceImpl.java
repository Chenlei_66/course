package com.course.system.service.impl;

import com.alibaba.fastjson.JSON;
import com.course.server.domain.Resource;
import com.course.server.domain.ResourceExample;
import com.course.server.dto.PageDTO;
import com.course.server.dto.ResourceDTO;
import com.course.server.mapper.ResourceMapper;
import com.course.server.util.CopyUtil;
import com.course.system.service.ResourceService;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;

import java.util.ArrayList;
import java.util.List;

@Service
public class ResourceServiceImpl implements ResourceService {

    private static final Logger LOG = LoggerFactory.getLogger(ResourceServiceImpl.class);

    @javax.annotation.Resource
    private ResourceMapper resourceMapper;

    /**
     * 列表查询
     *
     * @param pageDTO
     */
    @Override
    public void list(PageDTO<ResourceDTO> pageDTO) {
        PageHelper.startPage(pageDTO.getPage(), pageDTO.getSize());
        ResourceExample resourceExample = new ResourceExample();


        List<Resource> resourceList = resourceMapper.selectByExample(resourceExample);
        PageInfo<Resource> pageInfo = new PageInfo<>(resourceList);
        pageDTO.setTotal(pageInfo.getTotal());
        List<ResourceDTO> resourceDTOList = CopyUtil.copyList(resourceList, ResourceDTO.class);
        pageDTO.setList(resourceDTOList);
    }

    /**
     * 保存，id有值时更新，无值时新增
     *
     * @param resourceDTO
     * @return
     */
    @Override
    public boolean save(ResourceDTO resourceDTO) {
        Resource resource = CopyUtil.copy(resourceDTO, Resource.class);
        boolean result;
        if (StringUtils.isEmpty(resourceDTO.getId())) {
            result = insert(resource);
        } else {
            result = update(resource);
        }
        return result;
    }

    /**
     * 删除
     *
     * @param id
     * @return
     */
    @Override
    public boolean delete(String id) {
        return resourceMapper.deleteByPrimaryKey(id) == 1;
    }

    /**
     * 按Json批量保存
     *
     * @param jsonStr
     * @return
     */
    @Override
    @Transactional
    public boolean saveJson(String jsonStr) {
        List<ResourceDTO> jsonList = JSON.parseArray(jsonStr, ResourceDTO.class);
        List<ResourceDTO> list = new ArrayList<>();
        if (!CollectionUtils.isEmpty(jsonList)) {
            for (ResourceDTO d : jsonList) {
                d.setParent("");
                add(list, d);
            }
        }
        LOG.info("共{}条", list.size());

        resourceMapper.deleteByExample(null);
        for (int i = 0; i < list.size(); i++) {
            insert(CopyUtil.copy(list.get(i), Resource.class));
        }

        return true;
    }

    /**
     * 按约定将列表转换为资源树
     * 要求：ID要正序排列
     *
     * @return
     */
    @Override
    public List<ResourceDTO> loadTree() {
        ResourceExample resourceExample = new ResourceExample();
        resourceExample.setOrderByClause("id asc");
        List<Resource> resourceList = resourceMapper.selectByExample(resourceExample);
        List<ResourceDTO> resourceDTOList = CopyUtil.copyList(resourceList, ResourceDTO.class);
        for (int i = resourceDTOList.size() - 1; i >= 0; i--) {
            // 当前要移动的记录
            ResourceDTO child = resourceDTOList.get(i);

            // 如果当前节点没有父节点，就不用往下了
            if (StringUtils.isEmpty(child.getParent())) {
                continue;
            }

            // 查询父节点
            for (int j = i - 1; j >= 0; j--) {
                ResourceDTO parent = resourceDTOList.get(j);
                if (child.getParent().equals(parent.getId())) {
                    if (CollectionUtils.isEmpty(parent.getChildren())) {
                        parent.setChildren(new ArrayList<>());
                    }
                    // 添加到最前面，否则会变成倒序，因为循环是从后往前循环的
                    parent.getChildren().add(0, child);

                    // 子节点查找到父节点后，删除列表中的字节点
                    resourceDTOList.remove(child);
                }
            }
        }
        return resourceDTOList;
    }

    /**
     * 递归，将树形结果的节点全部取出来，放到list
     *
     * @param list
     * @param dto
     */
    private void add(List<ResourceDTO> list, ResourceDTO dto) {
        list.add(dto);
        if (!CollectionUtils.isEmpty(dto.getChildren())) {
            for (ResourceDTO d : dto.getChildren()) {
                d.setParent(dto.getId());
                add(list, d);
            }
        }
    }

    private boolean insert(Resource resource) {
        return resourceMapper.insert(resource) == 1;
    }

    private boolean update(Resource resource) {
        return resourceMapper.updateByPrimaryKey(resource) == 1;
    }
}
