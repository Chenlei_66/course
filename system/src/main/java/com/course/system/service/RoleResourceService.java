package com.course.system.service;

import com.course.server.dto.PageDTO;
import com.course.server.dto.RoleResourceDTO;

public interface RoleResourceService {
    /**
     * 列表查询
     * @param pageDTO
     */
    void list(PageDTO<RoleResourceDTO> pageDTO);

    /**
     * 保存，id有值时更新，无值时新增
     * @param roleResourceDTO
     * @return
     */
    boolean save(RoleResourceDTO roleResourceDTO);

    /**
     * 删除
     * @param id
     * @return
     */
    boolean delete(String id);
}
