package com.course.system.service.impl;

import com.course.server.domain.UserRole;
import com.course.server.domain.UserRoleExample;
import com.course.server.dto.PageDTO;
import com.course.server.dto.UserRoleDTO;
import com.course.server.mapper.UserRoleMapper;
import com.course.server.util.CopyUtil;
import com.course.server.util.UuidUtil;
import com.course.system.service.UserRoleService;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import javax.annotation.Resource;
import java.util.List;

@Service
public class UserRoleServiceImpl implements UserRoleService {
    @Resource
    private UserRoleMapper userRoleMapper;

    /**
     * 列表查询
     * @param pageDTO
     */
    @Override
    public void list(PageDTO<UserRoleDTO> pageDTO){
        PageHelper.startPage(pageDTO.getPage(),pageDTO.getSize());
        UserRoleExample userRoleExample = new UserRoleExample();


        List<UserRole> userRoleList = userRoleMapper.selectByExample(userRoleExample);
        PageInfo<UserRole> pageInfo = new PageInfo<>(userRoleList);
        pageDTO.setTotal(pageInfo.getTotal());
        List<UserRoleDTO> userRoleDTOList = CopyUtil.copyList(userRoleList, UserRoleDTO.class);
        pageDTO.setList(userRoleDTOList);
    }

    /**
     * 保存，id有值时更新，无值时新增
     * @param userRoleDTO
     * @return
     */
    @Override
    public boolean save(UserRoleDTO userRoleDTO) {
        UserRole userRole = CopyUtil.copy(userRoleDTO, UserRole.class);
        boolean result;
        if(StringUtils.isEmpty(userRoleDTO.getId())){
            result = insert(userRole);
        }else {
            result = update(userRole);
        }
        return result;
    }

    /**
     * 删除
     * @param id
     * @return
     */
    @Override
    public boolean delete(String id) {
        return userRoleMapper.deleteByPrimaryKey(id) == 1;
    }

    private boolean insert(UserRole userRole) {
        userRole.setId(UuidUtil.getShortUuid());
        return userRoleMapper.insert(userRole) == 1;
    }

    private boolean update(UserRole userRole) {
        return userRoleMapper.updateByPrimaryKey(userRole) == 1;
    }
}
