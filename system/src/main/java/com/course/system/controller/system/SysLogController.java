package com.course.system.controller.system;

import com.course.server.dto.PageDTO;
import com.course.server.dto.ResponseDTO;
import com.course.server.dto.SysLogDTO;
import com.course.server.dto.SysLogPageDTO;
import com.course.server.service.SysLogService;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

@RestController
@RequestMapping("/system/sysLog")
public class SysLogController {

    public static final String BUSINESS_NAME = "日志";

    @Resource
    private SysLogService sysLogService;

    /**
     * 列表查询
     * @param sysLogPageDTO
     * @return
     */
    @RequestMapping(value = "/list", method = RequestMethod.POST, produces = {"application/json;charset=UTF-8"})
    public ResponseDTO list(@RequestBody SysLogPageDTO sysLogPageDTO) {
        ResponseDTO<PageDTO<SysLogDTO>> responseDTO = new ResponseDTO<>();
        sysLogService.list(sysLogPageDTO);
        responseDTO.setContent(sysLogPageDTO);
        return responseDTO;
    }

}
