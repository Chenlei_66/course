package com.course.system.controller.admin;

import com.course.server.dto.PageDTO;
import com.course.server.dto.ResponseDTO;
import com.course.server.dto.RoleDTO;
import com.course.system.service.RoleService;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;

@RestController
@RequestMapping("admin/role")
public class RoleController {

    public static final String BUSINESS_NAME = "角色";

    @Resource
    private RoleService roleService;

    /**
     * 列表查询
     *
     * @param pageDTO
     * @return
     */
    @RequestMapping(value = "/list", method = RequestMethod.POST, produces = {"application/json;charset=UTF-8"})
    public ResponseDTO list(@RequestBody PageDTO<RoleDTO> pageDTO) {
        ResponseDTO<PageDTO<RoleDTO>> responseDTO = new ResponseDTO<>();
        roleService.list(pageDTO);
        responseDTO.setContent(pageDTO);
        return responseDTO;
    }

    /**
     * 保存，id有值时更新，无值时新增
     *
     * @param roleDTO
     * @return
     */
    @RequestMapping(value = "/save", method = RequestMethod.POST, produces = {"application/json;charset=UTF-8"})
    public ResponseDTO save(@RequestBody RoleDTO roleDTO) {
        // 保存校验

        ResponseDTO<RoleDTO> responseDTO = new ResponseDTO<>();
        boolean success = roleService.save(roleDTO);
        responseDTO.setSuccess(success);
        if (success) {
            responseDTO.setContent(roleDTO);
        }
        return responseDTO;
    }

    /**
     * 删除
     *
     * @param id
     * @return
     */
    @DeleteMapping(value = "/delete/{id}", produces = {"application/json;charset=UTF-8"})
    public ResponseDTO delete(@PathVariable String id) {
        ResponseDTO<RoleDTO> responseDTO = new ResponseDTO<>();
        boolean success = roleService.delete(id);
        responseDTO.setSuccess(success);
        return responseDTO;
    }

    /**
     * 保存资源
     *
     * @param roleDTO
     * @return
     */
    @PostMapping(value = "/saveResource", produces = {"application/json;charset=UTF-8"})
    public ResponseDTO saveResource(@RequestBody RoleDTO roleDTO) {
        ResponseDTO<RoleDTO> responseDTO = new ResponseDTO<>();
        roleService.saveResource(roleDTO);
        responseDTO.setContent(roleDTO);
        return responseDTO;
    }

    /**
     * 加载已关联的资源
     *
     * @param roleId
     * @return
     */
    @GetMapping(value = "/listResource/{roleId}", produces = {"application/json;charset=UTF-8"})
    public ResponseDTO listResource(@PathVariable String roleId) {
        ResponseDTO responseDTO = new ResponseDTO<>();
        List<String> resourceIdLit = roleService.listResource(roleId);
        responseDTO.setContent(resourceIdLit);
        return responseDTO;
    }

    /**
     * 保存资源
     *
     * @param roleDTO
     * @return
     */
    @PostMapping(value = "/saveUser", produces = {"application/json;charset=UTF-8"})
    public ResponseDTO saveUser(@RequestBody RoleDTO roleDTO) {
        ResponseDTO<RoleDTO> responseDTO = new ResponseDTO<>();
        roleService.saveUser(roleDTO);
        responseDTO.setContent(roleDTO);
        return responseDTO;
    }

    /**
     * 加载已关联的用户
     *
     * @param roleId
     * @return
     */
    @GetMapping(value = "/listUser/{roleId}", produces = {"application/json;charset=UTF-8"})
    public ResponseDTO listUser(@PathVariable String roleId) {
        ResponseDTO responseDTO = new ResponseDTO<>();
        List<String> resourceIdLit = roleService.listUser(roleId);
        responseDTO.setContent(resourceIdLit);
        return responseDTO;
    }
}
