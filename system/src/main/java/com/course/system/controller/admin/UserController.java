package com.course.system.controller.admin;

import com.alibaba.fastjson.JSON;
import com.course.server.dto.LoginUserDTO;
import com.course.server.dto.PageDTO;
import com.course.server.dto.ResponseDTO;
import com.course.server.dto.UserDTO;
import com.course.server.util.UuidUtil;
import com.course.server.util.ValidatorUtil;
import com.course.system.service.UserService;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.util.DigestUtils;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.concurrent.TimeUnit;

@RestController
@RequestMapping("admin/user")
public class UserController {
    private static final Logger LOG = LoggerFactory.getLogger(UserController.class);

    public static final String BUSINESS_NAME = "用户";

    @Resource
    private UserService userService;

    @Resource
    private RedisTemplate<String,String> redisTemplate;

    /**
     * 列表查询
     * @param pageDTO
     * @return
     */
    @RequestMapping(value = "/list", method = RequestMethod.POST, produces = {"application/json;charset=UTF-8"})
    public ResponseDTO list(@RequestBody PageDTO<UserDTO> pageDTO) {
        ResponseDTO<PageDTO<UserDTO>> responseDTO = new ResponseDTO<>();
        userService.list(pageDTO);
        responseDTO.setContent(pageDTO);
        return responseDTO;
    }

    /**
     * 保存，id有值时更新，无值时新增
     * @param userDTO
     * @return
     */
    @RequestMapping(value = "/save", method = RequestMethod.POST, produces = {"application/json;charset=UTF-8"})
    public ResponseDTO save(@RequestBody UserDTO userDTO) {
        // 后端再次用md5加密一次登录密码
        userDTO.setPassword(DigestUtils.md5DigestAsHex(userDTO.getPassword().getBytes()));
        // 保存校验
        ValidatorUtil.require(userDTO.getLoginName(),"登录名");
        ValidatorUtil.length(userDTO.getLoginName(),"登录名",1,50);
        ValidatorUtil.require(userDTO.getPassword(),"密码");
        ValidatorUtil.length(userDTO.getPassword(),"密码",1,32);

        ResponseDTO<UserDTO> responseDTO = new ResponseDTO<>();
        boolean success = userService.save(userDTO);
        responseDTO.setSuccess(success);
        if(success){
            responseDTO.setContent(userDTO);
        }
        return responseDTO;
    }

    /**
     * 删除
     * @param id
     * @return
     */
    @DeleteMapping(value = "/delete/{id}", produces = {"application/json;charset=UTF-8"})
    public ResponseDTO delete(@PathVariable String id) {
        ResponseDTO<UserDTO> responseDTO = new ResponseDTO<>();
        boolean success = userService.delete(id);
        responseDTO.setSuccess(success);
        return responseDTO;
    }

    /**
     * 重置密码
     * @param userDTO
     * @return
     */
    @RequestMapping(value = "/savePassword", method = RequestMethod.POST, produces = {"application/json;charset=UTF-8"})
    public ResponseDTO savePassword(@RequestBody UserDTO userDTO){
        userDTO.setPassword(DigestUtils.md5DigestAsHex(userDTO.getPassword().getBytes()));
        ResponseDTO<UserDTO> responseDTO = new ResponseDTO<>();
        boolean success = userService.savePassword(userDTO);
        responseDTO.setSuccess(success);
        if(success){
            responseDTO.setContent(userDTO);
        }
        return responseDTO;
    }

    /**
     * 用户登录
     * @param userDTO
     * @return
     */
    @RequestMapping(value = "/login", method = RequestMethod.POST, produces = {"application/json;charset=UTF-8"})
    public ResponseDTO<LoginUserDTO> login(@RequestBody UserDTO userDTO){
        userDTO.setPassword(DigestUtils.md5DigestAsHex(userDTO.getPassword().getBytes()));
        ResponseDTO<LoginUserDTO> responseDTO = new ResponseDTO<>();

        // 根据验证码token去获取缓存中的验证码，和用户输入是否一致
        // LOG.info("sessionId：{}",request.getSession().getId());
        // String imageCode = (String) request.getSession().getAttribute(userDTO.getImageCodeToken());

        String imageCode = redisTemplate.opsForValue().get(userDTO.getImageCodeToken());
        if(StringUtils.isEmpty(imageCode)) {
            LOG.info("用户登录失败，验证码已过期");
            responseDTO.setSuccess(false);
            responseDTO.setMessage("验证码已过期");
            return responseDTO;
        }
        if(!imageCode.toLowerCase().equals(userDTO.getImageCode().toLowerCase())){
            LOG.info("用户登录失败，验证码错误");
            responseDTO.setSuccess(false);
            responseDTO.setMessage("验证码错误");
            return responseDTO;
        } else {
            // 验证成功后，移除验证码
            // request.getSession().removeAttribute(userDTO.getImageCodeToken());
            redisTemplate.delete(userDTO.getImageCodeToken());
        }

        LoginUserDTO loginUserDTO = userService.login(userDTO);
        // 登录成功后缓存用户信息
        String token = UuidUtil.getShortUuid();
        loginUserDTO.setToken(token);
        // request.getSession().setAttribute(LoginConstants.LOGIN_USER, loginUserDTO);
        redisTemplate.opsForValue().set(token, JSON.toJSONString(loginUserDTO),3600, TimeUnit.SECONDS);

        responseDTO.setContent(loginUserDTO);
        return responseDTO;
    }

    /**
     * 退出登录
     * @return
     */
    @RequestMapping(value = "/logout/{token}", method = RequestMethod.GET, produces = {"application/json;charset=UTF-8"})
    public ResponseDTO<LoginUserDTO> logout(@PathVariable String token){
        ResponseDTO<LoginUserDTO> responseDTO = new ResponseDTO<>();
        // request.getSession().removeAttribute(LoginConstants.LOGIN_USER);
        redisTemplate.delete(token);
        LOG.info("从redis中删除token，{}",token);
        return responseDTO;
    }
}
