package com.course.system.controller.admin;

import com.course.server.dto.PageDTO;
import com.course.server.dto.ResponseDTO;
import com.course.server.dto.RoleResourceDTO;
import com.course.system.service.RoleResourceService;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;

@RestController
@RequestMapping("admin/roleResource")
public class RoleResourceController {

    public static final String BUSINESS_NAME = "角色资源";

    @Resource
    private RoleResourceService roleResourceService;

    /**
     * 列表查询
     * @param pageDTO
     * @return
     */
    @RequestMapping(value = "/list", method = RequestMethod.POST, produces = {"application/json;charset=UTF-8"})
    public ResponseDTO list(@RequestBody PageDTO<RoleResourceDTO> pageDTO) {
        ResponseDTO<PageDTO<RoleResourceDTO>> responseDTO = new ResponseDTO<>();
        roleResourceService.list(pageDTO);
        responseDTO.setContent(pageDTO);
        return responseDTO;
    }

    /**
     * 保存，id有值时更新，无值时新增
     * @param roleResourceDTO
     * @return
     */
    @RequestMapping(value = "/save", method = RequestMethod.POST, produces = {"application/json;charset=UTF-8"})
    public ResponseDTO save(@RequestBody RoleResourceDTO roleResourceDTO) {
        // 保存校验

        ResponseDTO<RoleResourceDTO> responseDTO = new ResponseDTO<>();
        boolean success = roleResourceService.save(roleResourceDTO);
        responseDTO.setSuccess(success);
        if(success){
            responseDTO.setContent(roleResourceDTO);
        }
        return responseDTO;
    }

    /**
     * 删除
     * @param id
     * @return
     */
    @DeleteMapping(value = "/delete/{id}", produces = {"application/json;charset=UTF-8"})
    public ResponseDTO delete(@PathVariable String id) {
        ResponseDTO<RoleResourceDTO> responseDTO = new ResponseDTO<>();
        boolean success = roleResourceService.delete(id);
        responseDTO.setSuccess(success);
        return responseDTO;
    }
}
