package com.course.system.controller.admin;

import com.course.server.dto.PageDTO;
import com.course.server.dto.ResponseDTO;
import com.course.server.dto.UserRoleDTO;
import com.course.system.service.UserRoleService;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;

@RestController
@RequestMapping("admin/userRole")
public class UserRoleController {

    public static final String BUSINESS_NAME = "用户角色";

    @Resource
    private UserRoleService userRoleService;

    /**
     * 列表查询
     * @param pageDTO
     * @return
     */
    @RequestMapping(value = "/list", method = RequestMethod.POST, produces = {"application/json;charset=UTF-8"})
    public ResponseDTO list(@RequestBody PageDTO<UserRoleDTO> pageDTO) {
        ResponseDTO<PageDTO<UserRoleDTO>> responseDTO = new ResponseDTO<>();
        userRoleService.list(pageDTO);
        responseDTO.setContent(pageDTO);
        return responseDTO;
    }

    /**
     * 保存，id有值时更新，无值时新增
     * @param userRoleDTO
     * @return
     */
    @RequestMapping(value = "/save", method = RequestMethod.POST, produces = {"application/json;charset=UTF-8"})
    public ResponseDTO save(@RequestBody UserRoleDTO userRoleDTO) {
        // 保存校验

        ResponseDTO<UserRoleDTO> responseDTO = new ResponseDTO<>();
        boolean success = userRoleService.save(userRoleDTO);
        responseDTO.setSuccess(success);
        if(success){
            responseDTO.setContent(userRoleDTO);
        }
        return responseDTO;
    }

    /**
     * 删除
     * @param id
     * @return
     */
    @DeleteMapping(value = "/delete/{id}", produces = {"application/json;charset=UTF-8"})
    public ResponseDTO delete(@PathVariable String id) {
        ResponseDTO<UserRoleDTO> responseDTO = new ResponseDTO<>();
        boolean success = userRoleService.delete(id);
        responseDTO.setSuccess(success);
        return responseDTO;
    }
}
