package com.course.system.controller.admin;

import com.google.code.kaptcha.impl.DefaultKaptcha;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import javax.imageio.ImageIO;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.util.concurrent.TimeUnit;

/**
 * @author cl
 * @date 2020/9/29 20:27
 */
@RestController
@RequestMapping("/admin/kaptcha")
public class KaptchaController {
    private static final Logger LOG = LoggerFactory.getLogger(KaptchaController.class);

    public static final String BUSINESS_NAME = "验证码";

    @Resource
    private DefaultKaptcha defaultKaptcha;

    @Resource
    private RedisTemplate<String,String> redisTemplate;


    /**
     * 生成验证码
     *
     * @param imageCodeToken 验证码token
     * @return
     */
    @RequestMapping(value = "/imageCode/{imageCodeToken}", method = RequestMethod.GET, produces = {"application/json;charset=UTF-8"})
    public void imageCode(@PathVariable(value = "imageCodeToken") String imageCodeToken,
                          HttpServletRequest request, HttpServletResponse response) throws Exception {

        ByteArrayOutputStream jpegOutputStream = new ByteArrayOutputStream();
        try {
            // 生成字符串验证码
            String createText = defaultKaptcha.createText();
            // 将生成的字符串验证码放入会话缓存中，后续验证的时候用到
            // LOG.info("sessionId：{}",request.getSession().getId());
            // request.getSession().setAttribute(imageCodeToken, createText);

            // 将生成的字符串验证码放入会话缓存中，后续验证的时候用到
            redisTemplate.opsForValue().set(imageCodeToken, createText,300, TimeUnit.SECONDS);
            // 使用验证码字符串生成验证码图片
            BufferedImage challenge = defaultKaptcha.createImage(createText);
            ImageIO.write(challenge, "jpg", jpegOutputStream);
        } catch (IllegalArgumentException e) {
            response.sendError(HttpServletResponse.SC_NOT_FOUND);
            return;
        }
        // 定义response输出类型为iimage/jpeg类型，使用response输出流输出图片的byte数组
        byte[] captchaChallengeAsJpeg = jpegOutputStream.toByteArray();
        response.setHeader("Cache-Control", "no-store");
        response.setHeader("Pragma", "no-cache");
        response.setDateHeader("Expires", 0);
        response.setContentType("image/jpeg");
        ServletOutputStream responseOutputStream = response.getOutputStream();
        responseOutputStream.write(captchaChallengeAsJpeg);
        responseOutputStream.flush();
        responseOutputStream.close();
    }
}
