package com.course.system.controller.admin;

import com.course.server.dto.PageDTO;
import com.course.server.dto.ResourceDTO;
import com.course.server.dto.ResponseDTO;
import com.course.server.util.ValidatorUtil;
import com.course.system.service.ResourceService;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;

@RestController
@RequestMapping("admin/resource")
public class ResourceController {

    public static final String BUSINESS_NAME = "资源";

    @Resource
    private ResourceService resourceService;

    /**
     * 列表查询
     * @param pageDTO
     * @return
     */
    @RequestMapping(value = "/list", method = RequestMethod.POST, produces = {"application/json;charset=UTF-8"})
    public ResponseDTO list(@RequestBody PageDTO<ResourceDTO> pageDTO) {
        ResponseDTO<PageDTO<ResourceDTO>> responseDTO = new ResponseDTO<>();
        resourceService.list(pageDTO);
        responseDTO.setContent(pageDTO);
        return responseDTO;
    }

    /**
     * 按Json批量保存
     * @param jsonStr
     * @return
     */
    @RequestMapping(value = "/save", method = RequestMethod.POST, produces = {"application/json;charset=UTF-8"})
    public ResponseDTO save(@RequestBody String jsonStr) {
        // 保存校验
        ValidatorUtil.require(jsonStr, "资源");

        ResponseDTO<ResourceDTO> responseDTO = new ResponseDTO<>();
        boolean success = resourceService.saveJson(jsonStr);
        responseDTO.setSuccess(success);
        return responseDTO;
    }

    /**
     * 删除
     * @param id
     * @return
     */
    @DeleteMapping(value = "/delete/{id}", produces = {"application/json;charset=UTF-8"})
    public ResponseDTO delete(@PathVariable String id) {
        ResponseDTO<ResourceDTO> responseDTO = new ResponseDTO<>();
        boolean success = resourceService.delete(id);
        responseDTO.setSuccess(success);
        return responseDTO;
    }

    /**
     * 资源数查询
     * @return
     */
    @RequestMapping(value = "/loadTree", method = RequestMethod.GET, produces = {"application/json;charset=UTF-8"})
    public ResponseDTO loadTree(){
        ResponseDTO<List<ResourceDTO>> responseDTO = new ResponseDTO<>();
        List<ResourceDTO> resourceList = resourceService.loadTree();
        responseDTO.setContent(resourceList);
        return responseDTO;
    }
}
